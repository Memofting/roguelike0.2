using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Application.RequestState;
using SFML.System;

namespace RogueLikeGame0._2.Application
{
    public static class GameMaster
    {

        private static EventHandler<EventArgs> _globalEvent;
        static public IRequestStack<ApplicationState> RequestStack { get; }

        static GameMaster()
        {
            RequestStack = new RequestStack(
                handler => _globalEvent += handler,
                handler => _globalEvent -= handler,
                () => _globalEvent?.GetInvocationList()
            );
        }
        
        public static void OnEvent(object sender, EventArgs args)
        {
            _globalEvent?.Invoke(sender, args);
        }

        public static void Run()
        {
            var clock = new Clock();
            clock.Restart();

            while (RequestStack.Count > 0)
            {
                var handler = RequestStack.PeakHandler();
                if (!handler.Update(clock.ElapsedTime))
                    RequestStack.TryPop(out _);
                clock.Restart();
                
                Thread.Sleep(50);
            }
        }
    }
    
    
}