using System;
using System.Linq;
using Ninject;
using Ninject.Extensions.Conventions;
using RogueLikeGame0._2.Application.RequestState;
using RogueLikeGame0._2.Domain.Implementation;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.InputController;
using RogueLikeGame0._2.Domain.Logic.EventHandling;
using RogueLikeGame0._2.Domain.MiniBar;
using RogueLikeGame0._2.Domain.PlayerController;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using SFML.Graphics;
using SFML.Window;

namespace RogueLikeGame0._2.Application.Masters
{
    public class DungeonMaster : IEventHandler, Drawable
    {
        private readonly GameContext _context;
        private Game Game { get; set; }
        private MiniBar MiniBar { get; }

        private StandardKernel container = new StandardKernel();

        private void AddBindings(View gameView, View miniMapView)
        {
            container.Bind(x =>
               x.FromThisAssembly()
               .SelectAllClasses()
               .InheritedFrom<IPlayerController>().BindAllInterfaces());
            container.Bind(x =>
                x.FromThisAssembly()
                .SelectAllClasses()
                .InheritedFrom<IMapGenerator>().BindAllInterfaces());
            container.Bind<DungeonMaster>().ToConstant(this);
            container.Bind<Game>().ToSelf()
                .WithConstructorArgument("gameView", gameView)
                .WithConstructorArgument("miniMapView", miniMapView);
            container.Bind<Content.DungeonType>().ToConstant(Content.DungeonType.Lobby);
            container.Bind<Scene>().ToMethod(c => CreateNewScene(
                c.Kernel.Get<Content.DungeonType>(),
                c.Kernel.Get<IMapGenerator>(),
                gameView,
                miniMapView,
                c.Kernel.Get<IPlayerController>()));
            container.Bind<IMapGenerator>().To<MapGenerator>()
                .WithConstructorArgument("lifeProbability", 0.45)
                .WithConstructorArgument("deathLimit", 3)
                .WithConstructorArgument("birthLimit", 4)
                .WithConstructorArgument("numberOfSteps", 3)
                .WithConstructorArgument("firstDimension", 50)
                .WithConstructorArgument("secondDimension", 50);
        }

        public DungeonMaster(
            View gameView,
            View miniMapView)
        {
            _context = new GameContext();
            Console.WriteLine($"enter to dungeon: {Content.DungeonType.Lobby.ToString()}");
            // game uses subscribeKeyPressedEvents
            AddBindings(gameView, miniMapView);
            Game = container.Get<Game>();
            MiniBar = new MiniBar();
        }

        private Scene CreateNewScene(Content.DungeonType dungeonType, IMapGenerator mapGenerator, View gameView, View miniMapView, IPlayerController playerController)
        {
            var scene = new Scene(_context, dungeonType, mapGenerator, playerController);

            foreach (var levelSelector in scene.ActiveGameObjects.Iterate().OfType<LevelSelector>())
                levelSelector.ZoneComponent.OnUpdate += guest =>
                {
                    if (guest is Player)
                        OnGameOver += () =>
                        {
                            Console.WriteLine($"enter to dungeon: {levelSelector.DungeonType.ToString()}");
                            Game.ChangeScene(CreateNewScene(levelSelector.DungeonType, mapGenerator, gameView, miniMapView, playerController));

                        };
                };
            foreach (var player in scene.ActiveGameObjects.Iterate().OfType<Player>())
            {
                player.DestroyableComponent.OnDeath += _ =>
                {
                    OnGameOver += () =>
                    {
                        GameMaster.RequestStack.Pop();
                    };
                };
            }
            return scene;
        }

        public void ChangeScore(int score)
        {
            MiniBar.CoinsScore.ChangeScore(score);
        }

        private event Action OnGameOver;
        public bool TryStopGame()
        {
            if (OnGameOver is null) return true;
            
            OnGameOver.Invoke();
            // так как игра заканчивается только один раз на каждом
            // подземелье, то нам надо гарантироват, что предыдущие
            // игры не помешают текущей, поэтому удаляем все события
            // с нимим связанные
            foreach (var d in OnGameOver.GetInvocationList())
                OnGameOver -= (Action) d;
            MiniBar.CoinsScore.ChangeScore(0);

            return true;
        }

        public void HandleEvent(object sender, EventArgs eventArgs)
        {
            Game.HandleEvent(sender, eventArgs);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            Game.Draw(target, states);
        }

        public void UpdateLogic()
        {
            Game.UpdateLogic();
        }

        public void UpdateAnimation(double elapsedMilliseconds)
        {
            Game.UpdateAnimation(elapsedMilliseconds);
        }
    }
}