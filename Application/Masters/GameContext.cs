using System;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.TacticsClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Domain.PlayerController;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Logic.Tactics;
using SFML.Graphics;

namespace RogueLikeGame0._2.Application.Masters
{
    public class GameContext : IGameContext
    {
        public Sprite GetSprite() => Infrastructure.Content.Content.CreateTileSprite();
        public IBehaviour GetTactic<T>(T obj, params object[] args)
        {
            if (typeof(T) == typeof(Player))
                return new PlayerTactic(obj as Player, args[0] as IPlayerController);
            if (typeof(T) == typeof(Boss))
                return new BossTactics(obj as Boss);
            if (typeof(T) == typeof(Star))
                return new SquareTactic(obj as Star);
            if (typeof(T) == typeof(Digger))
                return new MixedRandom(obj as Digger);
            throw new ArgumentException($"Doesn't have supported tactics for type: {typeof(T)}");
        }

    }
}