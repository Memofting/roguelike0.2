using RogueLikeGame0._2.Domain.Logic.EventHandling;
using SFML.System;

namespace RogueLikeGame0._2.Application.RequestState
{
    public interface IApplicationHandler: IEventHandler
    {
        bool Update(Time time);
    }
}