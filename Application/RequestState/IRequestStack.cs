using System;
using RogueLikeGame0._2.UserInterface;

namespace RogueLikeGame0._2.Application.RequestState
{
    public interface IRequestStack<T>
    {
        T Pop();
        void Push(T _);
        bool TryPop(out T _);

        int Count { get; }
        IApplicationHandler PeakHandler();
        void RegisterStateHandler(ApplicationState state, IApplicationHandler handler);
    }
}