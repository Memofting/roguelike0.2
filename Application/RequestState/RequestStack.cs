using System;
using System.Collections.Generic;
using System.Linq;
using RogueLikeGame0._2.UserInterface;

namespace RogueLikeGame0._2.Application.RequestState
{
    public class RequestStack : Stack<ApplicationState>, IRequestStack<ApplicationState>
    {
        private readonly Action<EventHandler<EventArgs>> _subscribe;
        private readonly Action<EventHandler<EventArgs>> _unscribe;
        private readonly Func<Delegate[]> _getInvocationList;
        private readonly Stack<Delegate[]> _gameStateGlobalHandlers;
        private readonly Dictionary<ApplicationState, IApplicationHandler> _handlers;

        public RequestStack(
            Action<EventHandler<EventArgs>> subscribe,
            Action<EventHandler<EventArgs>> unscribe,
            Func<Delegate[]> getInvocationList)
        {
            _subscribe = subscribe;
            _unscribe = unscribe;
            _getInvocationList = getInvocationList;

            _gameStateGlobalHandlers = new Stack<Delegate[]>();
            _handlers = new Dictionary<ApplicationState, IApplicationHandler>();
        }
        
        public void RegisterStateHandler(ApplicationState state, IApplicationHandler handler)
        {
            _handlers[state] = handler;
        }

        public IApplicationHandler PeakHandler()
        {
            return _handlers[Peek()];
        }

        public new ApplicationState Pop()
        {
            var state = base.Pop();
            PopAction();
            return state;
        }


        public new bool TryPop(out ApplicationState state)
        {
            var res = base.TryPop(out state);
            if (!res) return res;
            PopAction();
            return res;
        }

        private void PopAction()
        {
            foreach (var eventHandler in _getInvocationList.Invoke())
                _unscribe?.Invoke(eventHandler as EventHandler<EventArgs>);
            if (!_gameStateGlobalHandlers.TryPop(out _)) return;
            var curState = Peek();
            _subscribe(_handlers[curState].HandleEvent);
        }

        public new void Push(ApplicationState state)
        {
            var invocationList = _getInvocationList.Invoke();
            if (!(invocationList is null))
            {
                _gameStateGlobalHandlers.Push(invocationList);
                foreach (var eventHandler in _getInvocationList.Invoke())
                    _unscribe?.DynamicInvoke(eventHandler);
            }
            _subscribe(_handlers[state].HandleEvent);
            base.Push(state);
        }
        
        
    }

    public enum ApplicationState
    {
        Menu, Dungeon, Credits
    }
}