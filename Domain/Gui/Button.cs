using System;
using RogueLikeGame0._2.Infrastructure.Content.Menu;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Gui
{
    public class Button: Component
    {
        private Text mText;
        private Sprite mSprite;
        private bool mIsToggle;

        private ButtonTextureHolder _textureHolder;
        
        public Button(Font font, ButtonTextureHolder textureHolder)
        {
            _textureHolder = textureHolder;
            mIsToggle = false;
            mSprite = new Sprite(textureHolder.texture, textureHolder.normalRect) {Scale = new Vector2f(5f, 1f)};
            mText = new Text("", font)
            {
                FillColor = Color.White
            };
        }

        public event Action CallBack;
        public void SetText(string text) => mText.DisplayedString = text;

        public override bool IsSelectable() => true;
        public override void Select()
        {
            base.Select();
            mSprite.TextureRect = _textureHolder.selectedRect;
        }

        public override void Deselect()
        {
            base.Deselect();
            mSprite.TextureRect = _textureHolder.normalRect;
        }

        public override void Activate()
        {
            base.Activate();
            CallBack?.Invoke();
            Deactivate();
            Deselect();
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
            
            mSprite.Draw(target, states);
            mText.Draw(target, states);
        }
    }
}