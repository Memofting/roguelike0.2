using System;
using SFML.Graphics;

namespace RogueLikeGame0._2.Domain.Gui
{
    public abstract class Component: Transformable, Drawable
    {
        protected bool mIsSelected;
        protected bool mIsActive;
        
        public abstract bool IsSelectable();
        public virtual bool IsSelected() => mIsSelected;
        public virtual bool HasSelection() => false;
        public virtual void HandleEvent(EventArgs eventArgs) {}

        public virtual void Select() => mIsSelected = true;
        public virtual void Deselect() => mIsSelected = false;

        public virtual bool IsActive() => mIsActive;
        public virtual void Activate() => mIsActive = true;
        public virtual void Deactivate() => mIsActive = false;
        public abstract void Draw(RenderTarget target, RenderStates states);
    }
}