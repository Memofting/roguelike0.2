using System;
using System.Collections.Generic;
using SFML.Graphics;
using SFML.Window;

namespace RogueLikeGame0._2.Domain.Gui
{
    public class Container : Component
    {
        private List<Component> mChildren;
        private int mSelectedChild;
        

        public override bool IsSelectable() => false;
        public override bool HasSelection() => mSelectedChild != -1;

        public void Pack(Component component)
        {
            mChildren.Add(component);
            if (!HasSelection() && component.IsSelectable())
                Select(mChildren.Count - 1);
        }

        private void Select(int index)
        {
            if (!mChildren[index].IsSelectable()) 
                return;
            if (HasSelection())
                mChildren[mSelectedChild].Deselect();
            mChildren[index].Select();
            mSelectedChild = index;
        }

        public Container()
        {
            mChildren = new List<Component>();
            mSelectedChild = -1;
        }

        public override void HandleEvent(EventArgs eventArgs)
        {
            if (HasSelection() && mChildren[mSelectedChild].IsActive())
                mChildren[mSelectedChild].HandleEvent(eventArgs);
            else if (eventArgs is KeyEventArgs keyEvent)
            {
                switch (keyEvent.Code)
                {
                    case Keyboard.Key.W:
                    case Keyboard.Key.Up:
                        SelectPrevious();
                        break;
                    case Keyboard.Key.S:
                    case Keyboard.Key.Down:
                        SelectNext();
                        break;
                    case Keyboard.Key.Enter:
                    case Keyboard.Key.Space:
                        if (HasSelection())
                            mChildren[mSelectedChild].Activate();
                        break;
                }
            }
        }

        public void SelectPrevious()
        {
            if (!HasSelection())
                return;
            var prev = mSelectedChild;
            do
            {
                prev = (prev + mChildren.Count - 1) % mChildren.Count;
            } while (!mChildren[prev].IsSelectable());
            Select(prev);
        }

        public void SelectNext()
        {
            if (!HasSelection())
                return;
            var next = mSelectedChild;
            do
            {
                next = (next + 1) % mChildren.Count;
            } while (!mChildren[next].IsSelectable());
            Select(next);
        }
        
        public override void Draw(RenderTarget target, RenderStates states)
        {
            foreach (var mChild in mChildren)
                target.Draw(mChild, states);
        }
    }
}