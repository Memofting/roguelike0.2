using SFML.Graphics;

namespace RogueLikeGame0._2.Domain.Gui
{
    public class Label: Component
    {
        protected Text mText;
        
        public Label(string text, Font font)
        {
            mText = new Text(text, font);
        }
        public override bool IsSelectable() => false;
        public override void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
            target.Draw(mText, states);
        }
        public void SetText(string text) => mText.DisplayedString = text;
    }
}