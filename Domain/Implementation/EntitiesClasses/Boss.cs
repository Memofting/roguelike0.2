﻿using System;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Implementation.TacticsClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    class Boss : GameObject, IActiveEntity, IRigid, IAnimated, IMovable, IDestroyable, IDestructive, IEnemy
    {
        private BossTactics BossTactics { get; }

        public Boss(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType) 
            : base(context, position,  onScene, dungeonType)
        {
            BossTactics = new BossTactics(this);

            AnimationComponent = new AnimationComponent(this, 
                Content.CreateBossAnimation(DrawComponent.Sprite),
                Content.CommonState.Default);

            var tactic = context.GetTactic(this);
            MoveComponent = new MoveComponent(this, tactic);
            DestructiveComponent = new DestructiveComponent(this, tactic);
            
            DestroyableComponent = new DestroyableComponent(this, 
                4,
                scene => DestroyableComponent.Health += 1,
                scene =>
                {
                    DestroyableComponent.Health -= 1;
                    AnimationComponent.ActiveAnimationState = Content.RageState.InRange;
                },
                scene =>
                {
                    scene.ActiveGameObjects[PositionComponent.OnScene.X, PositionComponent.OnScene.Y] = null;
                    
                    Console.WriteLine("Boss dead!");
                    var coin = new Coin(context, PositionComponent.OnScene, DrawComponent.Position, DungeonType);
                    coin.SpawnComponent.Update(scene);
                });
        }
        public override void Update(IScene scene)
        {
            DestructiveComponent.Update(scene);
            MoveComponent.Update(scene);
        }
        
        public AnimationComponent AnimationComponent { get; }
        public MoveComponent MoveComponent { get; }
        public DestroyableComponent DestroyableComponent { get; }
        public DestructiveComponent DestructiveComponent { get; }
    }
}
