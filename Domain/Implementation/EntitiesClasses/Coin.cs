using System;
using System.Collections.Generic;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    public class Coin: GameObject, IZone, IAnimated, ISpawnable
    {   
        public Coin(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType) 
            : base(context, position,  onScene, dungeonType)
        {
            ZoneComponent = new ZoneComponent(this, 
                movable =>
                {
                    if (!(movable is IAlly ally)) return;
                    ally.ScoreComponent.Update(ally.ScoreComponent.Score + 1);
                });
            AnimationComponent = new AnimationComponent(this, 
                Content.CreateCoinAnimation(DrawComponent.Sprite), 
                Content.CommonState.Default);
            SpawnComponent = new SpawnComponent(this);
        }
        
        public ZoneComponent ZoneComponent { get; }
        public AnimationComponent AnimationComponent { get; }
        public SpawnComponent SpawnComponent { get; set; }
    }    
}