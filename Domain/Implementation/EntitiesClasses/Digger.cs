﻿using System;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using RogueLikeGame0._2.Logic.Tactics;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    public class Digger : GameObject, IActiveEntity, IRigid, IAnimated, IMovable, IDestroyable, IDestructive, IEnemy
    {

        public Digger(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType) 
            : base(context, position,  onScene, dungeonType)
        {
            AnimationComponent = new AnimationComponent(this, 
                Content.CreateDiggerAnimation(DrawComponent.Sprite, DungeonType),
                Content.CommonState.Default);
            var tactic = context.GetTactic(this);
            MoveComponent = new MoveComponent(this, tactic);
            DestructiveComponent = new DestructiveComponent(this, tactic);
            
            DestroyableComponent = new DestroyableComponent(this, 
                1,
                scene => DestroyableComponent.Health += 1,
                scene => DestroyableComponent.Health -= 1,
                scene =>
                {
                    scene.ActiveGameObjects[PositionComponent.OnScene.X, PositionComponent.OnScene.Y] = null;
                    
                    Console.WriteLine("Digger dead!");
                    var coin = new Coin(context, PositionComponent.OnScene, DrawComponent.Position, DungeonType);
                    coin.SpawnComponent.Update(scene);
                });
        }

        public override void Update(IScene scene)
        {
            DestructiveComponent.Update(scene);
            MoveComponent.Update(scene);
        }

        public AnimationComponent AnimationComponent { get; }
        public MoveComponent MoveComponent { get; }
        public DestroyableComponent DestroyableComponent { get; }
        public DestructiveComponent DestructiveComponent { get; }
    }
}
