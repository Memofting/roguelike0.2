using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    public class Floor: GameObject, IStaticEntity, IAnimated
    {        
        public Floor(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType, Content.Orientation orientation) 
            : base(context, position, onScene, dungeonType)
        {
            var animation = Content.CreateFloorAnimation(DrawComponent.Sprite, dungeonType, orientation);
            AnimationComponent = new AnimationComponent(this, 
                animation, 
                Content.StaticAnimationState.Up);
        }
        
        public Floor(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType)
            : base(context, position,  onScene, dungeonType)
        {
            var animation = Content.CreateFloorAnimation(DrawComponent.Sprite, dungeonType);
            AnimationComponent = new AnimationComponent(this,
                animation,
                Content.StaticAnimationState.Up);
        }

        public AnimationComponent AnimationComponent { get; }
    }
}