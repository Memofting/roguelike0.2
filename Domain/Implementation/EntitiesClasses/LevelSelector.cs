using System;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    public class LevelSelector : GameObject, IZone, IAnimated
    {
        public LevelSelector(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType) 
            : base(context, position,  onScene, dungeonType)
        {   
            ZoneComponent = new ZoneComponent(this, movable => { });
            var animation = Content.CreateLevelSelectorAnimation(DrawComponent.Sprite, dungeonType);
            AnimationComponent = new AnimationComponent(this, animation, Content.StaticAnimationState.Up);
        }
        public ZoneComponent ZoneComponent { get; }
        public AnimationComponent AnimationComponent { get; }
    }
}