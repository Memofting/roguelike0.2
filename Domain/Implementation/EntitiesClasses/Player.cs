using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Implementation.TacticsClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.EventHandling;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Domain.PlayerController;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    public class Player : GameObject, IActiveEntity, IRigid, IAnimated, IMovable, IDestroyable, IDestructive, IAlly
    {
        private readonly IBehaviour _tactic;
        public Player(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType, IPlayerController playerController) 
            : base(context, position,  onScene, dungeonType)
        {            
            _tactic = context.GetTactic(this, playerController);
            MoveComponent = new MoveComponent(this, _tactic);
            DestructiveComponent = new DestructiveComponent(this, _tactic);
            DestroyableComponent = new DestroyableComponent(this, 3, 
                scene => DestroyableComponent.Health += 1, 
                scene =>
                {
                     DestroyableComponent.Health -= 1;
                     DrawComponent.Sprite.Color += Settings.TransparentRed;
                },
                scene =>
                {
                    Console.WriteLine("Player dead!");
                    scene.ActiveGameObjects[PositionComponent.OnScene.X, PositionComponent.OnScene.Y] = null;
                });
            AnimationComponent = new AnimationComponent(this, 
                Content.CreatePlayerAnimations(DrawComponent.Sprite), 
                Content.MoveAnimationState.StandUp);
            ScoreComponent = new ScoreComponent(this, 0);
        }

        public override void Update(IScene scene)
        {
            DestructiveComponent.Update(scene);
            MoveComponent.Update(scene);
        }

        public override void HandleEvent(object sender, EventArgs eventArgs)
        {
            (_tactic as IEventHandler)?.HandleEvent(sender, eventArgs);
        }

        public AnimationComponent AnimationComponent { get; }
        public MoveComponent MoveComponent { get; }
        public DestroyableComponent DestroyableComponent { get; }
        public DestructiveComponent DestructiveComponent { get; }
        public ScoreComponent ScoreComponent { get; }
    }

}