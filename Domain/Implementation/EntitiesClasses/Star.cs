using System;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Implementation.TacticsClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    public class Star: GameObject, IActiveEntity, IRigid, IMovable, IAnimated, IDestroyable, IDestructive, IEnemy
    {
        public Star(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType) 
            : base(context, position, onScene, dungeonType)
        {
            var tactic = context.GetTactic(this);
            MoveComponent = new MoveComponent(this, tactic);
            DestructiveComponent = new DestructiveComponent(this, tactic);
            DestroyableComponent = new DestroyableComponent(this, 
                1, 
                scene => { }, 
                scene => DestroyableComponent.Health -= 1, 
                scene =>
                {
                    scene.ActiveGameObjects[PositionComponent.OnScene.X, PositionComponent.OnScene.Y] = null;
                    
                    Console.WriteLine("Star dead!");
                    var coin = new Coin(context, PositionComponent.OnScene, DrawComponent.Position, DungeonType);
                    coin.SpawnComponent.Update(scene);
                });
            AnimationComponent = new AnimationComponent(this, 
                Content.CreateStarAnimation(DrawComponent.Sprite, DungeonType),
                Content.CommonState.Default);
        }

        public override void Update(IScene scene)
        {
            DestructiveComponent.Update(scene);
            MoveComponent.Update(scene);
        }
        public MoveComponent MoveComponent { get; }
        public AnimationComponent AnimationComponent { get; }
        public DestroyableComponent DestroyableComponent { get; }
        public DestructiveComponent DestructiveComponent { get; }
    }
}