using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.EntitiesClasses
{
    public class Wall: GameObject, IStaticEntity, IRigid, IAnimated
    {
        public Wall(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType) : 
            base(context, position, onScene, dungeonType)
        {
            var animation = Content.CreateWallAnimation(DrawComponent.Sprite, dungeonType);
            AnimationComponent = new AnimationComponent(this, 
                animation,
                Content.StaticAnimationState.Up);
        }

        public AnimationComponent AnimationComponent { get; }
    }
}