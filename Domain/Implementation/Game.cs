using System;
using System.Collections.Generic;
using System.Linq;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.Masters;
using SFML.Graphics;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EventHandling;

namespace RogueLikeGame0._2.Domain.Implementation
{
    public class Game : IEventHandler, Drawable
    {
        private Scene _scene;
        private Player _player;
        private readonly View[] ViewPorts;
        private readonly DungeonMaster DungeonMaster;
        private readonly List<ViewComponent> playerObservers;

        public Game(DungeonMaster dungeonMaster, View gameView, View miniMapView, Scene scene)
        {
            ViewPorts = new[] { gameView, miniMapView };
            playerObservers = new List<ViewComponent>();
            DungeonMaster = dungeonMaster;
            ChangeScene(scene);
        }

        public void ChangeScene(Scene scene)
        {
            _scene = scene;
            _player = _scene.GetEntities<Player>().FirstOrDefault();
            if (_player is null) return;

            _player.DestroyableComponent.OnDeath += _ => DungeonMaster.TryStopGame();
            _player.ScoreComponent.OnUpdate += DungeonMaster.ChangeScore;

            foreach (var viewPort in ViewPorts)
            {
                var observer = new ViewComponent(viewPort);
                playerObservers.Add(observer);
                observer.Update(_player);
            }
        }

        public void UpdateLogic()
        {   
            _scene.UpdateLogic();
            
            foreach (var observer in playerObservers)
                observer.Update(_player);
        }
        
        public void UpdateAnimation(double elapsed)
        {
            _scene.UpdateAnimation(elapsed);
        }

        public void HandleEvent(object sender, EventArgs eventArgs)
        {
            _scene.HandleEvent(sender, eventArgs);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            _scene.Draw(target, states);
        }
    }
}