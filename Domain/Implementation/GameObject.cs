using System;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.EventHandling;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation
{
    public abstract class GameObject: Transformable, IGameObject, IDrawable, IHasPosition, IEventHandler
    {
        protected GameObject(IGameContext context, Vector2f position, Vector2i onScene, Content.DungeonType dungeonType)
        {
            DungeonType = dungeonType;
            DrawComponent = new DrawComponent(this, context.GetSprite(), position);
            PositionComponent = new PositionComponent(this, onScene);
        }
        public Content.DungeonType DungeonType { get; set; }
        public bool IsUpdated { get; set; }
        public virtual void Update(IScene scene) {}
        public DrawComponent DrawComponent { get; }
        public PositionComponent PositionComponent { get; }

        public virtual void HandleEvent(object sender, EventArgs eventArgs)
        {
        }
    }
}