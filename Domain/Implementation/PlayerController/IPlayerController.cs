﻿using SFML.Window;
using System;
using System.Collections.Generic;
using System.Text;
using static RogueLikeGame0._2.Infrastructure.Settings;

namespace RogueLikeGame0._2.Domain.PlayerController
{
    public interface IPlayerController
    {
        MoveDirection? TryGetMoveDirection(Keyboard.Key key);
        HitDirection? TryGetHitDirection(Keyboard.Key key);
    }
}
