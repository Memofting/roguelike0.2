﻿using System;
using System.Collections.Generic;
using System.Text;
using RogueLikeGame0._2.Domain.PlayerController;
using RogueLikeGame0._2.Infrastructure;
using SFML.System;
using SFML.Window;
using static RogueLikeGame0._2.Infrastructure.Settings;

namespace RogueLikeGame0._2.Domain.InputController
{
    public class KeyBoardPlayerController : IPlayerController
    {
        private IReadOnlyDictionary<Keyboard.Key, (Vector2i, Vector2f)> HitKeys;
        private IReadOnlyDictionary<Keyboard.Key, (Vector2i, Vector2f)> MoveKeys;

        public KeyBoardPlayerController(Dictionary<Keyboard.Key, (Vector2i, Vector2f)> hitKeys, Dictionary<Keyboard.Key, (Vector2i, Vector2f)> moveKeys)
        {
            HitKeys = hitKeys;
            MoveKeys = moveKeys;
        }

        public HitDirection? TryGetHitDirection(Keyboard.Key key)
        {
            switch (key)
            {
                case Keyboard.Key.W:
                    return HitDirection.Up;
                case Keyboard.Key.S:
                    return HitDirection.Down;
                case Keyboard.Key.A:
                    return HitDirection.Left;
                case Keyboard.Key.D:
                    return HitDirection.Right;
                default:
                    return null;
            }
        }

        public MoveDirection? TryGetMoveDirection(Keyboard.Key key)
        {
            switch (key)
            {
                case Keyboard.Key.Up:
                    return MoveDirection.Up;
                case Keyboard.Key.Down:
                    return MoveDirection.Down;
                case Keyboard.Key.Left:
                    return MoveDirection.Left;
                case Keyboard.Key.Right:
                    return MoveDirection.Right;
                default:
                    return null;
            }
        }
    }
}
