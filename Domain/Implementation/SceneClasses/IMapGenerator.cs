﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RogueLikeGame0._2.Domain.Implementation.SceneClasses
{
    public interface IMapGenerator
    {
        (char[,], char[,]) GetMap();
    }
}
