using System;
using System.Collections.Generic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.EventHandling;
using RogueLikeGame0._2.Logic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.SceneClasses
{
    public interface IScene: IEventHandler
    {
        IStaticEntity[,] StaticGameObjects { get; }
        IActiveEntity[,] ActiveGameObjects { get; }

        void Move(IActiveEntity self, Vector2i shift, Action callback);
        IActiveEntity this[Vector2i loc] { get; set; }

        IEnumerable<T> GetEntities<T>();
    }
}