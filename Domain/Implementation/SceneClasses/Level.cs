using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.PlayerController;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.SceneClasses
{
    public class Level
    {
        private static readonly ConcurrentDictionary<char, Func<object[], GameObject>> StaticStandardMapping;
        private static readonly ConcurrentDictionary<char, Func<object[], GameObject>> ActiveStandardMapping;
        private static readonly ConcurrentDictionary<char, Content.DungeonType> ExplicitDungeonTypes;

        static Level()
        {
            StaticStandardMapping = new ConcurrentDictionary<char, Func<object[], GameObject>>
            {
                ['#'] = CreateInstance<Wall>,
                [' '] = CreateInstance<Floor>,
            };
            ActiveStandardMapping = new ConcurrentDictionary<char, Func<object[], GameObject>>
            {
                ['.'] = CreateInstance<Player>,
                ['S'] = CreateInstance<Star>,
                ['C'] = CreateInstance<Coin>,
                ['0'] = CreateInstance<LevelSelector>,
                ['1'] = CreateInstance<LevelSelector>,
                ['2'] = CreateInstance<LevelSelector>,
                ['3'] = CreateInstance<LevelSelector>,
                ['B'] = CreateInstance<Boss>,
                ['D'] = CreateInstance<Digger>,
            };
            ExplicitDungeonTypes = new ConcurrentDictionary<char, Content.DungeonType>
            {
                ['0'] = Content.DungeonType.Lobby,
                ['1'] = Content.DungeonType.DungeonGrass,
                ['2'] = Content.DungeonType.DungeonStone,
                ['3'] = Content.DungeonType.DungeonBoss
            };
        }

        public static (IStaticEntity[,], IActiveEntity[,]) InitMap(IGameContext context, Content.DungeonType dungeonType, IMapGenerator mapGenerator, IPlayerController playerController)
        {
            switch (dungeonType)
            {
                case Content.DungeonType.Lobby:
                    return (
                        GetGameCells<IStaticEntity>(context, "lobby.static.txt", StaticStandardMapping,
                            dungeonType),
                        GetGameCells<IActiveEntity>(context, "lobby.active.txt", ActiveStandardMapping,
                            dungeonType, playerController));
                case Content.DungeonType.DungeonBoss:
                    return (
                        GetGameCells<IStaticEntity>(context, "boss.static.txt", StaticStandardMapping,
                            dungeonType),
                        GetGameCells<IActiveEntity>(context, "boss.active.txt", ActiveStandardMapping,
                            dungeonType, playerController));
                case Content.DungeonType.DungeonGrass:
                case Content.DungeonType.DungeonStone:
                default:
                    var (staticMap, activeMap) = mapGenerator.GetMap();
                    return (
                        GetGameCells<IStaticEntity>(context, staticMap, StaticStandardMapping, dungeonType),
                        GetGameCells<IActiveEntity>(context, activeMap, ActiveStandardMapping, dungeonType, playerController));
            }
        }

        private static T[,] GetGameCells<T>(
            IGameContext context,
            char[,] rowMap,
            IReadOnlyDictionary<char, Func<object[], GameObject>> mapping,
            Content.DungeonType dungeonType,
            IPlayerController playerController = null)
            where T : class, IGameObject, IHasPosition, IDrawable
        {
            return InitGameCells<T>(context, rowMap, mapping, dungeonType, playerController);
        }

        private static T[,] GetGameCells<T>(
            IGameContext context,
            string levelName,
            IReadOnlyDictionary<char, Func<object[], GameObject>> mapping,
            Content.DungeonType dungeonType,
            IPlayerController playerController = null)
            where T : class, IGameObject, IHasPosition, IDrawable
        {
            T[,] gameCells;
            if (!Content.TryGetLevelFileName(levelName, out var absLevelName))
                throw new FileNotFoundException($"Not such file for scene loading: {levelName}");
            using (var file = new StreamReader(absLevelName))
            {
                var ln = file.ReadLine();
                if (ln == null) return null;
                var arr = Array.ConvertAll(ln.Split(' '), int.Parse);
                var firstDimension = arr[0];
                var secondDimension = arr[1];
                var rowMap = new char[secondDimension, firstDimension];
                for (var i = 0; i < secondDimension; ++i)
                {
                    ln = file.ReadLine();
                    for (var j = 0; j < firstDimension; ++j)
                        rowMap[i, j] = ln[j];
                }
                gameCells = InitGameCells<T>(context, rowMap, mapping, dungeonType, playerController);
            }

            return gameCells;
        }

        private static T[,] InitGameCells<T>(
            IGameContext context, 
            char[,] rowMap,
            IReadOnlyDictionary<char, Func<object[], GameObject>> mapping,
            Content.DungeonType defaultDungeonType,
            IPlayerController playerController)
        
            where T: class, IGameObject, IHasPosition, IDrawable
        {
            var td = new TileOrientationDeterminator(rowMap);
            var gameCells = new T[rowMap.GetLength(0), rowMap.GetLength(1)];
            for (var i = 0; i < rowMap.GetLength(0); ++i)
            {
                for (var j = 0; j < rowMap.GetLength(1); ++j)
                {
                    var ch = rowMap[i, j];
                    if (!mapping.ContainsKey(ch))
                        continue;

                    var (onMap, onDisplay) = CreatePosition(j, i);
                    var args = new object[] {context, onMap, onDisplay, defaultDungeonType};
                    if (ExplicitDungeonTypes.TryGetValue(ch, out var explicitDungeonType))
                        args = new object[] {context, onMap, onDisplay, explicitDungeonType};
                    if (rowMap[i, j] == ' ' && defaultDungeonType == Content.DungeonType.DungeonGrass)
                        args = new object[] {context, onMap, onDisplay, defaultDungeonType, td.GetTileOrientation(new Vector2i(j, i)) };
                    if (rowMap[i, j] == '.')
                        args = new object[] { context, onMap, onDisplay, defaultDungeonType, playerController};
                    var gameCell = mapping[ch].Invoke(args) as T;
                    
                    gameCells[i, j] = gameCell;
                }
            }
            return gameCells.Transpose();
        }

        public static T CreateInstance<T>(object[] paramArray)
        {
            return (T)Activator.CreateInstance(typeof(T), paramArray);
        }

        private static (Vector2i, Vector2f) CreatePosition(int x, int y)
        {
            
            var onMap = new Vector2i(x, y);
            var onDisplay =
                new Vector2f(
                    onMap.X * Settings.CellWidth,
                    onMap.Y * Settings.CellHeight);
            return (onMap, onDisplay);
        }
    }

    public static partial class Array2DExtension
    {
        public static bool Accessible<T>(this T[,] arr, int i1, int i2)
        {
            return i1 >= 0 && i1 < arr.GetLength(0) && i2 >= 0 && i2 < arr.GetLength(1);
        }

        public static T[,] Transpose<T>(this T[,] arr)
        {
            var newArr = new T[arr.GetLength(1), arr.GetLength(0)];
            for (var i = 0; i < newArr.GetLength(0); ++i)
                for (var j = 0; j < newArr.GetLength(1); ++j)
                    newArr[i, j] = arr[j, i];
            return newArr;
        }
    }
}
