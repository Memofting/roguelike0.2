﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.SceneClasses
{
    class MapGenerator : IMapGenerator
    {
        private double LifeProbability;
        private int deathLimit;
        private int birthLimit;
        private int numberOfSteps;

        private int FirstDimension;
        private int SecondDimension;

        private bool[,] Map;

        private readonly char[,] StaticMap;
        private readonly char[,] ActiveMap;

        private static readonly ConcurrentDictionary<Type, char> mapping = new ConcurrentDictionary<Type, char>
        {
            [typeof(Player)] = '.',
            [typeof(Boss)] = 'B',
            [typeof(Star)] = 'S',
            [typeof(Digger)] = 'D',
            [typeof(LevelSelector)] = '0',
        };

        public MapGenerator(
                double lifeProbability,
                int deathLimit,
                int birthLimit,
                int numberOfSteps,
                int firstDimension,
                int secondDimension)
        {
            LifeProbability = lifeProbability;
            this.deathLimit = deathLimit;
            this.birthLimit = birthLimit;
            this.numberOfSteps = numberOfSteps;

            FirstDimension = firstDimension;
            SecondDimension = secondDimension;

            StaticMap = new char[FirstDimension, SecondDimension];
            ActiveMap = new char[FirstDimension, SecondDimension];
        }

        public (char[,], char[,]) GetMap()
        {
            var map = GenerateMap();

            var caves = new Caves(map);
            Map = caves.ConnectCaves();

            var rnd = new Random();
            var startPosition = caves.CavesCoordinates.First()[rnd.Next(0, caves.CavesCoordinates.First().Count)];
            var endPosition = caves.CavesCoordinates.Last()[rnd.Next(0, caves.CavesCoordinates.Last().Count)];

            for (int x = 0; x < FirstDimension; x++)
            {
                for (int y = 0; y < SecondDimension; y++)
                {
                    StaticMap[x, y] = Map[x, y] ? '#' : ' ';
                    ActiveMap[x, y] = Map[x, y] ? '#' : ' ';
                }
            }

            ActiveMap[startPosition.X, startPosition.Y] = mapping[typeof(Player)];
            ActiveMap[endPosition.X, endPosition.Y] = mapping[typeof(LevelSelector)];

            AddEntity(caves.CavesCoordinates, typeof(Star), radius => (int)(SecondDimension * FirstDimension * LifeProbability) / (radius * radius));
            AddEntity(caves.CavesCoordinates, typeof(Digger), radius => (int)(Math.Min(FirstDimension, SecondDimension) * LifeProbability / radius));

            return (StaticMap, ActiveMap);
        }

        private void AddEntity(IReadOnlyList<IReadOnlyList<Vector2i>> caves, Type entity, Func<int, int> getQuantity)
        {
            var radius = 5;
            var quantity = getQuantity(radius);
            var rnd = new Random();
            while (quantity != 0)
            {
                var cave = caves[rnd.Next(caves.Count)];
                var nextPosition = cave[rnd.Next(cave.Count)];
                if (!IsFreeInRadius(nextPosition, radius)) continue;
                ActiveMap[nextPosition.X, nextPosition.Y] = mapping[entity];
                quantity--;
            }
        }

        private bool IsFreeInRadius(Vector2i position, int radius)
        {
            for (var i = -radius / 2; i <= radius / 2; i++)
                for (var j = -radius / 2; j <= radius / 2; j++)
                {
                    var newY = i + position.X;
                    var newX = j + position.Y;
                    if (newY < 0 || newX < 0 || newY >= FirstDimension || newX >= SecondDimension)
                        continue;
                    if (ActiveMap[newY, newX] != ' ' &&
                        ActiveMap[newY, newX] != '#')
                        return false;
                }
            return true;
        }

        private bool[,] FillMap(bool[,] map)
        {
            var rand = new Random();
            for (int y = 0; y < FirstDimension; y++)
                for (int x = 0; x < SecondDimension; x++)
                    if (rand.NextDouble() < LifeProbability)
                        map[y, x] = true;

            return map;
        }

        private int CountNeighbours(bool[,] map, int y, int x)
        {
            int count = 0;
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++)
                {
                    int newY = y + i;
                    int newX = x + j;
                    if (i == 0 && j == 0)
                        continue;
                    if (newY < 0 || newX < 0 || newY >= FirstDimension || newX >= SecondDimension)
                        count = count + 1;
                    else if (map[newY, newX])
                        count = count + 1;
                }
            return count;
        }


        private bool[,] MakeStep(bool[,] map)
        {
            bool[,] newMap = new bool[FirstDimension, SecondDimension];
            for (int y = 0; y < FirstDimension; y++)
                for (int x = 0; x < SecondDimension; x++)
                {
                    int neighboursCounter = CountNeighbours(map, y, x);

                    if (map[y, x])
                    {
                        newMap[y, x] = true;
                        if (neighboursCounter < deathLimit)
                            newMap[y, x] = false;
                    }
                    else
                    {
                        newMap[y, x] = false;
                        if (neighboursCounter > birthLimit)
                            newMap[y, x] = true;
                    }
                }
            return newMap;
        }

        private bool[,] GenerateMap()
        {
            bool[,] Map = new bool[FirstDimension, SecondDimension];
            Map = FillMap(Map);
            for (int i = 0; i < numberOfSteps; i++)
                Map = MakeStep(Map);

            for (int i = 0; i < FirstDimension; i++)
            {
                Map[i, 0] = true;
                Map[i, SecondDimension - 1] = true;
            }

            for (int i = 0; i < SecondDimension; i++)
            {
                Map[0, i] = true;
                Map[FirstDimension - 1, i] = true;
            }

            return Map;
        }
    }

    internal class Caves
    {
        private bool[,] Map;
        private readonly int firstDimension;
        private readonly int secondDimension;

        public IReadOnlyList<IReadOnlyList<Vector2i>> CavesCoordinates;

        public Caves(bool[,] map)
        {
            Map = map;

            firstDimension = map.GetLength(0);
            secondDimension = map.GetLength(1);

            CavesCoordinates = GetCaves();
        }

        private IReadOnlyList<IReadOnlyList<Vector2i>> GetCaves()
        {
            var caves = new List<List<Vector2i>>();
            var visited = new HashSet<Vector2i>();
            for (int y = 0; y < firstDimension; y++)
            {
                for (int x = 0; x < secondDimension; x++)
                {
                    var nextCell = new Vector2i(y, x);
                    if (Map[y, x] || visited.Contains(nextCell)) continue;
                    var cave = GetCave(y, x);
                    if (cave.Count != 0) caves.Add(cave);
                    foreach (var node in cave)
                        visited.Add(node);
                }
            }
            return caves;
        }

        private List<Vector2i> GetCave(int y, int x)
        {
            var queue = new Queue<Vector2i>();
            var cave = new List<Vector2i>();
            queue.Enqueue(new Vector2i(y, x));
            while (queue.Count != 0)
            {
                var point = queue.Dequeue();
                if (point.X < 0 ||
                    point.X >= Map.GetLength(0) ||
                    point.Y < 0 ||
                    point.Y >= Map.GetLength(1))
                    continue;
                if (Map[point.X, point.Y] || cave.Contains(point))
                    continue;
                cave.Add(point);

                for (var dy = -1; dy <= 1; dy++)
                    for (var dx = -1; dx <= 1; dx++)
                        if (dx != 0 && dy != 0) continue;
                        else queue.Enqueue(new Vector2i(point.X + dx, point.Y + dy));
            }
            return cave;
        }

        public bool[,] ConnectCaves()
        {
            var separatedCaves = new HashSet<IReadOnlyList<Vector2i>>();
            foreach (var cave in CavesCoordinates)
                separatedCaves.Add(cave);
            var previous = separatedCaves.First();
            separatedCaves.Remove(previous);

            var newMap = new bool[firstDimension, secondDimension];
            for (int y = 0; y < firstDimension; y++)
                for (int x = 0; x < secondDimension; x++)
                    newMap[y, x] = Map[y, x];

            while (separatedCaves.Count != 0)
            {
                var current = separatedCaves.First();
                Connect(current, previous, newMap);
                previous = current;
                separatedCaves.Remove(previous);
            }

            return newMap;
        }

        private int GetDistance(Vector2i from, Vector2i to)
        {
            return (from.X - to.X) * (from.X - to.X) + (from.Y - to.Y) * (from.Y - to.Y);
        }

        private void Connect(IReadOnlyList<Vector2i> from, IReadOnlyList<Vector2i> to, bool[,] map)
        {
            var firstPoint = from.First();
            var secondPoint = to.First();

            var directions = new[] { new Vector2i(1, 0), new Vector2i(0, 1),
                    new Vector2i(-1, 0), new Vector2i(0, -1) };

            while (firstPoint != secondPoint)
            {
                var bestDirection = directions
                    .Select(direction => Tuple.Create(GetDistance(firstPoint + direction, secondPoint), direction))
                    .OrderBy(tuple => tuple.Item1)
                    .First()
                    .Item2;
                firstPoint += bestDirection;
                map[firstPoint.X, firstPoint.Y] = false;
            }
        }
    }
}
