using System;
using System.Collections.Generic;
using System.Linq;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.TacticsClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.EventHandling;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Domain.PlayerController;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic.MobTactic;
using SFML.Graphics;
using SFML.System;


namespace RogueLikeGame0._2.Domain.Implementation.SceneClasses
{
    public class Scene : Transformable, Drawable, IScene
    {
        public IStaticEntity[,] StaticGameObjects { get; }
        public IActiveEntity[,] ActiveGameObjects { get; }

        public Scene(IGameContext context, Content.DungeonType dungeonType,
            Func<IGameContext, Content.DungeonType, (IStaticEntity[,], IActiveEntity[,])> sceneInitializer)
        {
            (StaticGameObjects, ActiveGameObjects) = sceneInitializer(context, dungeonType);
        }
        public Scene(IGameContext context, Content.DungeonType dungeonType, IMapGenerator mapGenerator, IPlayerController playerController)
        {
            (StaticGameObjects, ActiveGameObjects) = Level.InitMap(context, dungeonType, mapGenerator, playerController);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;

            foreach (var gameObject in StaticGameObjects.Iterate().Where(s => !(s is null)))
                gameObject.DrawComponent.Update((target, states));
            foreach (var gameObject in ActiveGameObjects.Iterate().Where(s => !(s is null)))
                gameObject.DrawComponent.Update((target, states));
        }

        private void DirectMove(IActiveEntity self, Vector2i shift)
        {
            var newPos = self.PositionComponent.OnScene + shift;

            this[self.PositionComponent.OnScene] = null;
            this[newPos] = self;
            self.PositionComponent.Update(newPos);
        }

        public void Move(IActiveEntity self, Vector2i shift, Action callback)
        {
            var newPos = self.PositionComponent.OnScene + shift;

            // check boundaries;
            if (!ActiveGameObjects.Accessible(newPos.X, newPos.Y) ||
                !StaticGameObjects.Accessible(newPos.X, newPos.Y)) return;
            // check rigid body;
            if (StaticGameObjects[newPos.X, newPos.Y] is IRigid ||
                ActiveGameObjects[newPos.X, newPos.Y] is IRigid) return;
            // check pass through zone;
            if (ActiveGameObjects[newPos.X, newPos.Y] is IZone asZone)
                switch (self)
                {
                    case IEnemy _:
                        return;
                    case IMovable movable:
                        asZone.ZoneComponent.Update(movable);
                        break;
                }
            DirectMove(self, shift);
            callback.Invoke();
        }

        public IEnumerable<T> GetEntities<T>()
        {
            foreach (var obj in ActiveGameObjects.Iterate().OfType<T>())
                yield return obj;
            foreach (var obj in StaticGameObjects.Iterate().OfType<T>())
                yield return obj;
        }

        public void UpdateLogic()
        {
            Console.WriteLine("new loop");

            // todo: плохо, когда мы оповещаем тактики так, даже через статические поля, да и этот метод не для этого
            var plrPos = ActiveGameObjects.Iterate().Where(s => !(s is null)).Where(s => s is Player);
            if (plrPos.Count() != 0)
                GoToPlayer.TellMePosition(plrPos.First().PositionComponent.OnScene);

            BossTactics.TellStatObj(StaticGameObjects);
            BossTactics.TellActiveObj(ActiveGameObjects);

            foreach (var entity in StaticGameObjects.Iterate())
                if (entity is IGameObject gameObject) gameObject.Update(this);

            foreach (var entity in ActiveGameObjects.Iterate())
            {
                if (!(entity is IGameObject gameObject) || gameObject.IsUpdated) continue;
                gameObject.Update(this);
                gameObject.IsUpdated = true;
            }

            foreach (var entity in ActiveGameObjects.Iterate())
                if (entity is IGameObject gameObject) gameObject.IsUpdated = false;
        }

        public void UpdateAnimation(double elapsed)
        {
            foreach (var gameObject in ActiveGameObjects.Iterate().Where(s => !(s is null)))
                if (gameObject is IAnimated asAnimated)
                    asAnimated.AnimationComponent.Update(elapsed);
            foreach (var gameObject in StaticGameObjects.Iterate().Where(s => !(s is null)))
                if (gameObject is IAnimated asAnimated)
                    asAnimated.AnimationComponent.Update(elapsed);
        }

        public IActiveEntity this[Vector2i onScene]
        {
            get => ActiveGameObjects.InBoundaries(onScene) ? ActiveGameObjects[onScene.X, onScene.Y] : null;
            set
            {
                if (ActiveGameObjects.InBoundaries(onScene))
                    ActiveGameObjects[onScene.X, onScene.Y] = value;
            }
        }

        public void HandleEvent(object sender, EventArgs eventArgs)
        {
            foreach (var eventHandler in ActiveGameObjects.Iterate().OfType<IEventHandler>())
                eventHandler.HandleEvent(sender, eventArgs);
            foreach (var eventHandler in StaticGameObjects.Iterate().OfType<IEventHandler>())
                eventHandler.HandleEvent(sender, eventArgs);
        }
    }

    public static partial class Array2DExtension
    {
        public static IEnumerable<T> Iterate<T>(this T[,] arr)
        {
            for (var i = 0; i < arr.GetLength(0); ++i)
                for (var j = 0; j < arr.GetLength(1); ++j)
                    yield return arr[i, j];
        }

        public static bool InBoundaries<T>(this T[,] arr, Vector2i location)
        {
            return 0 <= location.X && location.X < arr.GetLength(0)
                   && 0 <= location.Y && location.Y < arr.GetLength(1);
        }
    }
}
