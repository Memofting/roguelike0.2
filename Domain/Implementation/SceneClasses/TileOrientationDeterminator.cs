﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RogueLikeGame0._2.Infrastructure.Content;

namespace RogueLikeGame0._2.Domain.Implementation.SceneClasses
{
    class TileOrientationDeterminator
    {
        private static Dictionary<int, Content.Orientation> Masks;
        private static char[,] Map;

        static TileOrientationDeterminator()
        {
            Masks = GetMasks("tileMasks.txt");
        }

        public TileOrientationDeterminator(char[,] map)
        {
            Map = map;
        }

        private static Dictionary<int, Content.Orientation> GetMasks(string fname)
        {
            var tile = new List<bool[]>();
            Dictionary<int, Content.Orientation> masks = new Dictionary<int, Content.Orientation>();
            using (var file = new StreamReader($"{Content.MapFolder}/{fname}"))
            {
                var orientation = file.ReadLine();
                var ln = file.ReadLine();
                while (orientation != null && ln != null)
                {
                    if (ln == "")
                    {
                        if (tile != null)
                        {
                            var hash = GetHash(tile.ToArray());
                            masks[hash] = (Content.Orientation)Enum.Parse(typeof(Content.Orientation), orientation);
                        }
                        tile = new List<bool[]>();
                        orientation = file.ReadLine();
                        ln = file.ReadLine();
                        continue;

                    }
                    var arr = ln.Split(' ').Select(s => s == "1").ToArray();
                    tile.Add(arr);

                    ln = file.ReadLine();
                }
            }
            return masks;
        }

        private static int GetHash(bool[][] matrix)
        {
            var hash = 0;
            var k = 0;
            for (int i = 0; i < matrix.Length; ++i)
                for (int j = 0; j < matrix.Length; ++j)
                {
                    if (matrix[i][j])
                        hash += (int)Math.Pow(2, k);
                    k++;
                }
            return hash;
        }

        public Content.Orientation GetTileOrientation(Vector2i position)
        {
            var neighborhood = new List<bool[]>();
            for (var i = Math.Max(0, position.X - 1); i < Math.Min(position.X + 2, Map.GetLength(0)); i++)
            {
                var line = new List<bool>();
                for (var j = Math.Max(0, position.Y - 1); j < Math.Min(position.Y + 2, Map.GetLength(1)); j++)
                {
                    if (i != position.X && j != position.Y)
                    {
                        line.Add(true);
                        continue;
                    }
                    line.Add(Map[j, i] == '#' ? false : true);
                }
                neighborhood.Add(line.ToArray());
            }

            var neighborhoodHash = GetHash(neighborhood.ToArray());
            if (Masks.Keys.Contains(neighborhoodHash))
                return Masks[neighborhoodHash];
            return Content.Orientation.Simple;
        }
    }
}
