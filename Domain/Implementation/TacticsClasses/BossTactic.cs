﻿using System;
using System.Collections.Generic;
using System.Linq;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Logic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.TacticsClasses
{
    class BossTactics : IBehaviour
    {
        private (Vector2i, Vector2f) _prevState;
        private Vector2i _prevPose;
        private bool _tacticStart;
        private int _state;
        private int _lastMoveDirection;
        private readonly IMovable _observeObject;
        private readonly Queue<(Vector2i, Vector2f)> _states = new Queue<(Vector2i, Vector2f)>();
        private readonly List<Vector2i> _directions = new List<Vector2i>{
            new Vector2i(0, -1), 
            new Vector2i(-1, 0), 
            new Vector2i(0, 1), 
            new Vector2i(1, 0) 
        };

        public BossTactics(IMovable observeObject)
        {
            _observeObject = observeObject;
            _lastMoveDirection = 0;
            _tacticStart = false;
            _state = -1;
        }

        private static IStaticEntity[,] _staticGameObjects;
        public static void TellStatObj(IStaticEntity[,] staticGameObjects)
        {
            _staticGameObjects = staticGameObjects;
        }

        private static IActiveEntity[,] _map;
        public static void TellActiveObj(IActiveEntity[,] activeGameObjects)
        {
            _map = activeGameObjects;
        }

        private Vector2i? TryFindPlayerInRadius()
        {
            var radius = 7;
            var position = _prevPose;
            //var map = _observeObject.Map.ActiveGameObjects;
            for (var i = -radius; i <= radius; i++)
                for (var j = -radius; j <= radius; j++)
                {
                    var newY = i + position.X;
                    var newX = j + position.Y;
                    if (newY < 0 || newX < 0 || newY >= _map.GetLength(0) || newX >= _map.GetLength(1))
                        continue;
                    if (_map[newY, newX] is Player)
                        return new Vector2i(newX, newY);
                }

            return null;
        }

        public Vector2i GetShiftForNextMove()
        {
            if (!_tacticStart)
                _tacticStart = true;
            if (_states.Count != 0)
            {
                var direction = _states.Dequeue();
                if (direction != Settings.MoveOnPlace)
                    _lastMoveDirection = _directions.IndexOf(direction.Item1);
                _prevPose = _observeObject.PositionComponent.OnScene;
                return direction.Item1;
            }
            var player = TryFindPlayerInRadius();

            if (player is null)
            {
                var rnd = new Random();
                _state = rnd.Next(4);
            }
            else
            {
                var bestDirection = _directions
                    .Select(direction => Tuple.Create(GetDistance(_observeObject.PositionComponent.OnScene + direction, player.Value), direction))
                    .OrderBy(tuple => tuple.Item1)
                    .SkipWhile(tuple =>
                    {
                        var (_, loc) = tuple;
                        var objLocation = new Vector2i(
                            loc.X + _observeObject.PositionComponent.OnScene.X,
                            loc.Y + _observeObject.PositionComponent.OnScene.Y);
                        return _staticGameObjects[objLocation.X, objLocation.Y] is Wall;
                    })
                    .First()
                    .Item2;

                _state = _directions.ToList().IndexOf(bestDirection);
            }
            if (_state != _lastMoveDirection)
            {
                if (Math.Abs(_state - _directions.IndexOf(_prevState.Item1)) == 2)
                    _states.Enqueue(Settings.MoveOnPlace);
                _states.Enqueue(GenerateNextMoveVectors());
                _prevState = Settings.MoveOnPlace;
            }
            else
            {
                _prevState = GenerateNextMoveVectors();
                _lastMoveDirection = _state;
            }
            _prevPose = _observeObject.PositionComponent.OnScene;
            return _prevState.Item1;
        }

        private int GetDistance(Vector2i from, Vector2i to) =>
            (from.X - to.Y) * (from.X - to.Y) + (from.Y - to.X) * (from.Y - to.X);

        public (Vector2i, Vector2f) GenerateNextMoveVectors()
        {
            switch (_state)
            {
                case 0:
                    return Settings.MoveUp;
                case 1:
                    return Settings.MoveLeft;
                case 2:
                    return Settings.MoveDown;
                case 3:
                    return Settings.MoveRight;
            }
            throw new NotImplementedException();
        }

        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            Console.WriteLine("Boss hits");
            for (var i = _prevPose.X - 1; i < _prevPose.X + 1; i++)
                for (var j = _prevPose.Y - 1; j < _prevPose.Y + 1; j++)
                    if (i >= 0 && j >= 0)
                        yield return new Vector2i(i, j) + _prevState.Item1;
        }
    }
}
