﻿using System;
using System.Collections.Generic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Infrastructure;
using SFML.System;

namespace RogueLikeGame0._2.Logic.MobTactic
{
    public class GoToPlayer : IBehaviour
    {
        private (Vector2i, Vector2f) _prevState;

        private Vector2i _prevPose;
        private static Vector2i playerPosition;

        private readonly IMovable _observeObject;

        public static void TellMePosition(Vector2i vector2F)
        {
            playerPosition = vector2F;
        }

        public GoToPlayer(IMovable observeObject)
        {
            _observeObject = observeObject;
        }


        public Vector2i GetShiftForNextMove()
        {
            _prevPose = _observeObject.PositionComponent.OnScene;
            var temp = playerPosition - _observeObject.PositionComponent.OnScene;

            if (Math.Abs(temp.X) > Math.Abs(temp.Y))
                _prevState = temp.X > 0 ? Settings.MoveRight : Settings.MoveLeft;
            else
                _prevState = temp.Y > 0 ? Settings.MoveDown : Settings.MoveUp;

            return _prevState.Item1;
        }

        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            yield return _prevPose + _prevState.Item1;
        }
    }
}
