﻿using RogueLikeGame0._2.Domain.Implementation.TacticsClasses;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Logic.MobTactic;
using SFML.System;
using System;
using System.Collections.Generic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;

namespace RogueLikeGame0._2.Logic.Tactics
{
    class MixedRandom : IBehaviour
    {
        private readonly IMovable _observeObject;
        private IBehaviour SecondBehaviour { get; set; }
        private IBehaviour FirstBehaviour { get; set; }
        private Random rnd = new Random(345);

        public MixedRandom(IMovable observeObject)
        {
            _observeObject = observeObject;
            FirstBehaviour = new GoToPlayer(observeObject);
            SecondBehaviour = new SquareTactic(observeObject);
        }

        private bool choise;

        public Vector2i GetShiftForNextMove()
        {
            choise = rnd.Next(1, 118) % 10 < 5;
            return choise ? FirstBehaviour.GetShiftForNextMove() : SecondBehaviour.GetShiftForNextMove();
        }

        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            return choise ? FirstBehaviour.GetHitObjectsLocations() : SecondBehaviour.GetHitObjectsLocations();
        }
    }
}
