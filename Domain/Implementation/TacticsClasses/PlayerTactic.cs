using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.EventHandling;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Domain.PlayerController;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using RogueLikeGame0._2.Logic.MobTactic;
using RogueLikeGame0._2.UserInterface;
using SFML.System;
using SFML.Window;

namespace RogueLikeGame0._2.Domain.Implementation.TacticsClasses
{
    public class PlayerTactic: IEventHandler, IBehaviour
    {
        public  (Vector2i, Vector2f) MoveTo { get; set; }
        public Vector2i? HitDirection { get; set; }
        private Player ObservableObject { get; }
        private IPlayerController PlayerController;

        public PlayerTactic(Player observableObject, IPlayerController playerController)
        {
            ObservableObject = observableObject;
            MoveTo = Settings.MoveOnPlace;
            PlayerController = playerController;
        }

//        public void KeyPressedEvent

        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            // todo: пока у нас только один способ удара - тычок в сторону
            if (HitDirection.HasValue)
                yield return ObservableObject.PositionComponent.OnScene + HitDirection.Value;
            HitDirection = null;
        }

        public Vector2i GetShiftForNextMove()
        {
            try
            {
                return MoveTo.Item1;
            }
            finally
            {
                MoveTo = Settings.MoveOnPlace;
            }
        }

        public void HandleEvent(object sender, EventArgs evArgs)
        {
            if (!(evArgs is KeyEventArgs args)) return;
            var hitCmd = PlayerController.TryGetHitDirection(args.Code);
            if (hitCmd != null)
                switch (hitCmd)
                {
                    case Settings.HitDirection.Up:
                        HitDirection = Settings.MoveUp.Item1;
                        ObservableObject.AnimationComponent.ActiveAnimationState = Content.HitAnimation.HitUp;
                        break;
                    case Settings.HitDirection.Down:
                        HitDirection = Settings.MoveDown.Item1;
                        ObservableObject.AnimationComponent.ActiveAnimationState = Content.HitAnimation.HitDown;
                        break;
                    case Settings.HitDirection.Left:
                        HitDirection = Settings.MoveLeft.Item1;
                        ObservableObject.AnimationComponent.ActiveAnimationState = Content.HitAnimation.HitLeft;
                        break;
                    case Settings.HitDirection.Right:
                        HitDirection = Settings.MoveRight.Item1;
                        ObservableObject.AnimationComponent.ActiveAnimationState = Content.HitAnimation.HitRight;
                        break;
                }
            else
            {
                var moveCmd = PlayerController.TryGetMoveDirection(args.Code);
                if (moveCmd != null)
                    switch (moveCmd)
                    {
                        case Settings.MoveDirection.Up:
                            MoveTo = Settings.MoveUp;
                            ObservableObject.AnimationComponent.ActiveAnimationState = Content.MoveAnimationState.Right;
                            break;
                        case Settings.MoveDirection.Down:
                            MoveTo = Settings.MoveDown;
                            ObservableObject.AnimationComponent.ActiveAnimationState = Content.MoveAnimationState.Left;
                            break;
                        case Settings.MoveDirection.Left:
                            MoveTo = Settings.MoveLeft;
                            ObservableObject.AnimationComponent.ActiveAnimationState = Content.MoveAnimationState.Left;
                            break;
                        case Settings.MoveDirection.Right:
                            MoveTo = Settings.MoveRight;
                            ObservableObject.AnimationComponent.ActiveAnimationState = Content.MoveAnimationState.Right;
                            break;
                    }
            }
        }
    }
}