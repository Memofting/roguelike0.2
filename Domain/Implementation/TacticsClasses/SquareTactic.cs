using System;
using System.Collections.Generic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Logic;
using RogueLikeGame0._2.Logic.MobTactic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Implementation.TacticsClasses
{
    public class SquareTactic : IBehaviour
    {
        public (Vector2i, Vector2f) MoveTo { get; set; }
        // todo: добавить поворот к entity (RotateComponent) для того, чтобы наш Star двигался всега прямо
        public Vector2i? HitDirection => MoveTo.Item1;

        private Vector2i _prevPose;
        private bool _tacticStart;
        private int _state;
        private readonly IMovable _observeObject;
        
        public SquareTactic(IMovable observeObject)
        {
            _observeObject = observeObject;
            _tacticStart = false;
            _state = -1;
        }

        public Vector2i GetShiftForNextMove()
        {
            if (!_tacticStart)
            {
                _tacticStart = true;
                _prevPose = _observeObject.PositionComponent.OnScene;   
                _state = (_state + 1) % 4;
                MoveTo = GenerateNextMoveVectors();
                return MoveTo.Item1;
            }
            if (_prevPose + MoveTo.Item1 != _observeObject.PositionComponent.OnScene)
                return MoveTo.Item1;
            
            _prevPose = _observeObject.PositionComponent.OnScene;   
            _state = (_state + 1) % 4;
            MoveTo = GenerateNextMoveVectors();
            return MoveTo.Item1;
        }

        public (Vector2i, Vector2f) GenerateNextMoveVectors()
        {
            switch (_state)
            {
                case 0:
                    return Settings.MoveUp;
                case 1:
                    return Settings.MoveLeft;
                case 2:
                    return Settings.MoveDown;
                case 3:
                    return Settings.MoveRight;
            }
            throw new NotImplementedException();
        }

        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            yield return _prevPose + MoveTo.Item1;
        }
    }
}