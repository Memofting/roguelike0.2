using System;
using System.Collections.Generic;
using System.Linq;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using SFML.Graphics;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class AnimationComponent: BaseComponent<IAnimated, double>
    {
        private Dictionary<Enum, Animation> AnimationDict { get; }
        private Enum _activeAnimationState;
        public Enum ActiveAnimationState {
            private get { return _activeAnimationState; }
            set
            {
                if (AnimationDict.TryGetValue(ActiveAnimationState, out var animation))
                    animation.Reset();
                _activeAnimationState = value;
            }
        }

        public AnimationComponent(IAnimated observableObject, Dictionary<Enum, Animation> animationDict, Enum activeAnimationState) : base(observableObject)
        {
            AnimationDict = animationDict;
            _activeAnimationState = activeAnimationState;

            OnUpdate += (elapsed) =>
            {
                if (animationDict.TryGetValue(ActiveAnimationState, out var animation))
                    animation.Update(elapsed);
            };
        }
    }
    

//    public enum ActiveAnimationState
//    {
//        Left, Right, HitLeft, HitRight, HitUp, HitDown,
//    }
//
//    public enum StaticAnimationState
//    {
//        
//    }

    public struct Frame
    {
        public IntRect Rect;
        public readonly double Duration;

        public Frame(double duration, IntRect rect)
        {
            Duration = duration;
            Rect = rect;
        }
    }
    
    public enum PlayBackMode
    {
        RepeatOnce, Loop, Swing
    }

    public class Animation: ICloneable
    {
        private readonly PlayBackMode _playBackMode;
        private readonly List<Frame> _frames;
        private readonly Sprite _target;
        
        private double _totalLength;
        private double _progress;

        public Animation(Sprite target, PlayBackMode playBackMode = PlayBackMode.RepeatOnce)
        {
            _target = target;
            _playBackMode = playBackMode;
                
            _progress = _totalLength = 0.0;
            _frames = new List<Frame>();
        }

        public void AddFrame(Frame frame)
        {
            _totalLength += frame.Duration;
            _frames.Add(frame);

            _target.TextureRect = frame.Rect;
        }

        public void Update(double elapsed)
        {
            _progress += elapsed;
            var p = _progress;
            if (p / _totalLength > 1.0)
                if (_playBackMode == PlayBackMode.Loop 
                    || _playBackMode == PlayBackMode.Swing)
                    p %= _totalLength;
                else
                    return;

            
            for (var i = 0; i < _frames.Count; i++)
            {
                p -= _frames[i].Duration;

                // if we have progressed OR if we're on the last frame, apply and stop.
                if (!(p <= 0.0) && !_frames[i].Equals(_frames.Last())) continue;
                _target.TextureRect = _frames[i].Rect;

                if (p > 0.0 && _frames[i].Equals(_frames.Last()))
                    switch (_playBackMode)
                    {
                        case PlayBackMode.Swing:
                            i = 0;
                            _frames.Reverse();
                            break;
                        case PlayBackMode.Loop:
                            i = 0;
                            break;
                    }
                else
                    break; // we found our frame
            }
        }

        public void Reset()
        {
            _progress =  0.0;
        }

        public object Clone()
        {
            var animation = new Animation(new Sprite(_target), _playBackMode);
            foreach (var frame in _frames)
                animation.AddFrame(frame);
            return animation;
        }
    }
}