using System;
using RogueLikeGame0._2.Logic;

namespace RogueLikeGame0._2.Domain.Logic
{
    public abstract class BaseComponent<T1, T2>
    {
        protected T1 ObservableObject { get; }
        protected BaseComponent(T1 observableObject)
        {
            ObservableObject = observableObject;
        }
        public event Action<T2> OnUpdate;
        public void Update(T2 interactObject) => OnUpdate?.Invoke(interactObject);
    }
}