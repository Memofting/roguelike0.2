using System;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class DestroyableComponent: BaseComponent<IDestroyable, IScene>
    {
        public event Action<IScene> OnIncreaseHealh;
        public event Action<IScene> OnDecreseHealh;
        public event Action<IScene> OnDeath;
        public double Health { get; set; }
        
        public DestroyableComponent(
            IDestroyable observableObject, 
            double health, 
            Action<IScene> increase,
            Action<IScene> decrease,
            Action<IScene> death) : base(observableObject)
        {
            Health = health;
            OnIncreaseHealh += increase;
            OnDecreseHealh += decrease;
            OnDeath += death;
            
            OnUpdate += scene =>
            {
                Console.WriteLine($"{ObservableObject.GetType()} get damage");
                OnDecreseHealh?.Invoke(scene);
                if (Health > 0) return;
                OnDeath?.Invoke(scene);
            };
        }
    }
}