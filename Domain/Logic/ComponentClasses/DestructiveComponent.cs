using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class DestructiveComponent: BaseComponent<IDestructive, IScene>
    {
        private static bool handleConflictPossibility(object damager, object damageReciever)
        {
            return !(damager is IEnemy && damageReciever is IEnemy);
        }

        public DestructiveComponent(IDestructive observableObject, IHitTactic hitTactic) : base(observableObject)
        {
            OnUpdate += scene =>
            {
                foreach (var loc in hitTactic.GetHitObjectsLocations())
                {
                    var cell = scene.ActiveGameObjects[loc.X, loc.Y];

                    bool conflictPossible = handleConflictPossibility(observableObject, cell);

                    if (cell is IDestroyable asDestroyable && !ReferenceEquals(observableObject, cell) && conflictPossible)
                        asDestroyable.DestroyableComponent.Update(scene);
                }
            };
        }
    }
}