using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Logic;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class DrawComponent: BaseComponent<IDrawable, (RenderTarget, RenderStates)>
    {
        
        public Sprite Sprite { get; private set; }
        public Vector2f Position
        {
            get => Sprite.Position;
            set => Sprite.Position = value;
        }
        
        public DrawComponent(IDrawable observableObject, Sprite sprite, Vector2f position) : base(observableObject)
        {
            Sprite = sprite;
            Position = position;
            OnUpdate += renderInfo =>
            {
                var (target, states) = renderInfo;
                target.Draw(Sprite, states);
            };
        }
    }
}