using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Logic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class MoveComponent: BaseComponent<IActiveEntity, IScene>
    {
        public Vector2i MoveShift { get; set; }
        public MoveComponent(IActiveEntity observableObject, IMoveTactic moveTactic) : base(observableObject)
        {
            MoveShift = new Vector2i(0, 0);
            
            OnUpdate += scene =>
            {
                MoveShift = moveTactic.GetShiftForNextMove();
                scene.Move(
                    ObservableObject, MoveShift,
                    // todo: подумать, как убрать эту зависимость
                    () => ObservableObject.DrawComponent.Position += new Vector2f(MoveShift.X * Settings.CellWidth, MoveShift.Y * Settings.CellHeight));
            };
        }
    }
}