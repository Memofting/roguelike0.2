using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class PositionComponent: BaseComponent<IHasPosition, Vector2i>
    {
        public Vector2i OnScene { get; private set; }
        public PositionComponent(IHasPosition observableObject, Vector2i onScene) : base(observableObject)
        {
            OnScene = onScene;
            OnUpdate += newOnScene => OnScene = newOnScene;
        }
    }
}