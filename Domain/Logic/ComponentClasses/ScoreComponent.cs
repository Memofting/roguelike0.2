using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class ScoreComponent: BaseComponent<IHasScore, int>
    {
        public int Score { get; private set; }

        public ScoreComponent(IHasScore observableObject, int initScore) : base(observableObject)
        {
            Score = initScore;
            OnUpdate += newScore => Score = newScore;
        }
    }
}