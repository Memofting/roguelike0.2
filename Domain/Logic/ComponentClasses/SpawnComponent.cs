using System;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Logic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class SpawnComponent: BaseComponent<ISpawnable, IScene>
    {
        // SpawnComponent пытается переместить объект, а не затереть тот, что стоит на этой позиции
        public SpawnComponent(ISpawnable observableObject) : base(observableObject)
        {
            OnUpdate += scene => scene[ObservableObject.PositionComponent.OnScene] = ObservableObject;
        }
    }
}