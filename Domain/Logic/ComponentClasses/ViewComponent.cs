using System.Collections.Generic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using SFML.Graphics;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class ViewComponent: BaseComponent<View, IDrawable>
    {
        public ViewComponent(View observableObject) : base(observableObject)
        {
            OnUpdate += drawable => ObservableObject.Center = drawable.DrawComponent.Position;
        }
    }
}