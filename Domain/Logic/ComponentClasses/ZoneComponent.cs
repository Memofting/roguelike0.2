using System;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;

namespace RogueLikeGame0._2.Domain.Logic
{
    public class ZoneComponent: BaseComponent<IZone, IMovable>
    {
        public ZoneComponent(IZone observableObject, Action<IMovable> enter) : base(observableObject)
        {
            OnUpdate += enter;
        }
    }
}