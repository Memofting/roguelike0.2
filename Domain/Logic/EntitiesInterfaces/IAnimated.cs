namespace RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces
{
    public interface IAnimated : IDrawable
    {
        AnimationComponent AnimationComponent { get; }
    }
}