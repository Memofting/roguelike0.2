namespace RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces
{
    public interface IDestructive
    {
        DestructiveComponent DestructiveComponent { get; }
    }
}