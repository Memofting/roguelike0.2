using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Infrastructure.Content;

namespace RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces
{
    public interface IGameObject
    {
        Content.DungeonType DungeonType { get; set; }
        bool IsUpdated { get; set; }
        void Update(IScene scene);
    }
}