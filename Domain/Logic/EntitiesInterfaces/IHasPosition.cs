namespace RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces
{
    public interface IHasPosition
    {
        PositionComponent PositionComponent { get; }
    }
}