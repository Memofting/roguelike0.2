namespace RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces
{
    public interface IMovable : IHasPosition
    {
        MoveComponent MoveComponent { get; }
    }
}