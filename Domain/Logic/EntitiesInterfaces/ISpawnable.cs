namespace RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces
{
    public interface ISpawnable : IActiveEntity
    {
        SpawnComponent SpawnComponent { get; set; }
    }
}