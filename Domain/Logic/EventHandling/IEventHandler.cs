using System;

namespace RogueLikeGame0._2.Domain.Logic.EventHandling
{
    public interface IEventHandler
    {
        void HandleEvent(object sender, EventArgs eventArgs);
    }
}