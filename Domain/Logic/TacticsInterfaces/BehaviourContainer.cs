using System;
using System.Collections.Generic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Logic.TacticsInterfaces
{
    public class BehaviourContainer : IBehaviour
    {
        private readonly Func<IEnumerable<Vector2i>> _getHitObjectsLocations;
        private readonly Func<Vector2i> _getShiftForNextMove;
        
        public BehaviourContainer(IMoveTactic moveTactic)
        {
            _getShiftForNextMove = moveTactic.GetShiftForNextMove;
        }

        public BehaviourContainer(IHitTactic hitTactic)
        {
            _getHitObjectsLocations = hitTactic.GetHitObjectsLocations;
        }

        public BehaviourContainer(IMoveTactic moveTactic, IHitTactic hitTactic)
        {
            _getShiftForNextMove = moveTactic.GetShiftForNextMove;
            _getHitObjectsLocations = hitTactic.GetHitObjectsLocations;
        }

        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            return _getHitObjectsLocations.Invoke();
        }

        public Vector2i GetShiftForNextMove()
        {
            return _getShiftForNextMove.Invoke();
        }
    }
}