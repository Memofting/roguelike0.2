﻿namespace RogueLikeGame0._2.Domain.Logic.TacticsInterfaces
{
    public interface IBehaviour : IHitTactic, IMoveTactic
    {
    }
}
