using System.Collections.Generic;
using RogueLikeGame0._2.Logic.MobTactic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Logic.TacticsInterfaces
{
    public interface IHitTactic: ITactic
    {
        IEnumerable<Vector2i> GetHitObjectsLocations();
    }
}