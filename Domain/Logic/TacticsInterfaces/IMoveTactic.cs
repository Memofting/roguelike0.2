using RogueLikeGame0._2.Logic.MobTactic;
using SFML.System;

namespace RogueLikeGame0._2.Domain.Logic.TacticsInterfaces
{
    public interface IMoveTactic: ITactic
    {
        Vector2i GetShiftForNextMove();
    }
}