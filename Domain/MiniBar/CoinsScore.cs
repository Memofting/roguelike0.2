using RogueLikeGame0._2.Infrastructure.Content;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Domain.MiniBar
{
    public class CoinsScore: Drawable
    {
        private Sprite CoinsSprite { get; }
        private Text CoinsScoreTxt { get; }
        private Vector2f ViewSize { get; set; }

        public CoinsScore()
        {
            CoinsSprite = Content.CreateCoinViewSprite();
            CoinsScoreTxt = Content.CoinText;
            
            CoinsScoreTxt.DisplayedString = "x0";
            ViewSize = new Vector2f();
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            if (ViewSize != target.GetView().Size)
                ChangeGuiView(target.GetView().Size);
            CoinsSprite.Draw(target, states);
            CoinsScoreTxt.Draw(target, states);
        }

        public void ChangeGuiView(Vector2f size) {
            ViewSize = size;

            // coins
            var coinsTextOffset = new Vector2f(CoinsSprite.TextureRect.Width + 10, 0);
            var coinsPosition = new Vector2f(
                0.98f * ViewSize.X
                - CoinsSprite.TextureRect.Width
                - coinsTextOffset.X
                - CoinsScoreTxt.GetGlobalBounds().Width, 0);
            CoinsSprite.Position = coinsPosition;
            CoinsScoreTxt.Position = coinsPosition + coinsTextOffset;
        }

        public void ChangeScore(int score)
        {
            CoinsScoreTxt.DisplayedString = $"x{score}";
        }
    }
}