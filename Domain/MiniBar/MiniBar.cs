using SFML.Graphics;

namespace RogueLikeGame0._2.Domain.MiniBar
{
    public class MiniBar: Drawable
    {
        public CoinsScore CoinsScore { get; set; }
        

        public MiniBar()
        {
            // на монетки и жизни мы выделим по 50% выданного пространтсва
            CoinsScore = new CoinsScore();
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            CoinsScore.Draw(target, states);
        }
    }
}