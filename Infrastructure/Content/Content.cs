using System;
using System.Collections.Generic;
using System.IO;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Infrastructure.Content.Menu;
using SFML.Audio;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Infrastructure.Content
{
    public static class Content
    {
        private const string ContentFolder = "Infrastructure/Content";
        public const string MapFolder = ContentFolder + "/MapDescriptions";
        private const string CoinFolder = ContentFolder + "/Coins";
        private const string MenuFolder = ContentFolder + "/Menu";
        
        public static readonly Text CoinText;
        
        public static readonly Font MenuFont;
        
        private static readonly Texture PlayerTexture;
        private static readonly Texture TileTexture;
        private static readonly Texture CoinsTexture;

        static Content()
        {
            PlayerTexture = new Texture($"{ContentFolder}/simple_character_2.png");
            TileTexture = new Texture($"{ContentFolder}/decoracy.png");
            CoinsTexture = new Texture($"{CoinFolder}/golden_coin.png");
            CoinText = new Text
            {
                Font = new Font($"{CoinFolder}/Gula.ttf"),
                CharacterSize = 50,
                FillColor = new Color(207, 181, 59)
            };
            MenuFont = new Font($"{MenuFolder}/Dungeon.ttf");
        }
        
        public static Sprite CreateColoredSprite(Color color)
        {
            var sprite = CreateTileSprite();
            sprite.TextureRect = new IntRect(0, 0, 32, 32);
            sprite.Color = color;
            sprite.Scale = new Vector2f(50f, 50f);
            return sprite;
        }

        public static ButtonTextureHolder CreateButtonTextureHolder()
        {
            return new ButtonTextureHolder
            {
                texture = new Texture(TileTexture),
                normalRect =  new IntRect(608, 64, 32, 32),
                selectedRect = new IntRect(577, 64, 32, 32),
            };
        }

        public static bool TryGetLevelFileName(string fileName, out string absFileName)
        {
            var res = Directory.GetFiles(MapFolder, fileName).Length == 1;
            absFileName = null;
            if (res)
                absFileName = $"{MapFolder}/{fileName}";
            return res;
        }

        public static Sprite CreateCoinViewSprite()
        {
            var sprite = CreateTileSprite();
            sprite.TextureRect = new IntRect(320, 3040, 17, 17);
            return sprite;
        }
        public static Sprite CreateTileSprite() => new Sprite(TileTexture)
        {
            Scale = new Vector2f(2f, 2f)
        };

        public static Dictionary<Enum, Animation> CreateCoinAnimation(Sprite coinSprite)
        {
            var rotationAnimation = new Animation(coinSprite, PlayBackMode.Swing);
            for (var i=0; i<7; ++i)
                rotationAnimation.AddFrame(
                    new Frame(150, 
                    new IntRect(320 + i * 17, 3040, 17, 17)));

            return new Dictionary<Enum, Animation>
            {
                [CommonState.Default] = rotationAnimation
            };
        }

        public static Dictionary<Enum, Animation> CreateBossAnimation(Sprite bossSprite)
        {
            var rightAnimation = new Animation(bossSprite);
            rightAnimation.AddFrame(new Frame(1, new IntRect(385, 3008, 32, 32)));

            var rageAnimation = new Animation(bossSprite);
            rageAnimation.AddFrame(new Frame(1, new IntRect(417, 3008, 32, 32)));

            return new Dictionary<Enum, Animation>
            {
                [MoveAnimationState.Right] = rightAnimation,
                [RageState.InRange] = rageAnimation,
            };
        }

        public static Dictionary<Enum, Animation> CreatePlayerAnimations(Sprite playerSprite)
        {
            playerSprite.Scale = new Vector2f(1, 1);
            
            var rightAnimation = new Animation(playerSprite);
            rightAnimation.AddFrame(new Frame(100, new IntRect(0, 3040, 64, 64)));
            
            var leftAnimation = new Animation(playerSprite);
            leftAnimation.AddFrame(new Frame(100, new IntRect(64, 3040, 64, 64)));
            
            var hitLeft = new Animation(playerSprite);
            hitLeft.AddFrame(new Frame(80, new IntRect(0, 3104, 64, 64)));
            hitLeft.AddFrame(new Frame(80, new IntRect(64, 3104, 64, 64)));
            hitLeft.AddFrame(new Frame(80, new IntRect(128, 3104, 64, 64)));
            hitLeft.AddFrame(new Frame(80, new IntRect(192, 3104, 64, 64)));
            hitLeft.AddFrame(new Frame(80, new IntRect(128, 3104, 64, 64)));
            hitLeft.AddFrame(new Frame(80, new IntRect(64, 3040, 64, 64)));

            var hitRight = new Animation(playerSprite);
            hitRight.AddFrame(new Frame(80, new IntRect(192, 3168, 64, 64)));
            hitRight.AddFrame(new Frame(80, new IntRect(128, 3168, 64, 64)));
            hitRight.AddFrame(new Frame(80, new IntRect(64, 3168, 64, 64)));
            hitRight.AddFrame(new Frame(80, new IntRect(0, 3168, 64, 64)));
            hitRight.AddFrame(new Frame(80, new IntRect(64, 3168, 64, 64)));
            hitRight.AddFrame(new Frame(80, new IntRect(0, 3040, 64, 64)));

            var hitUp = new Animation(playerSprite);
            hitUp.AddFrame(new Frame(80, new IntRect(0, 3232, 64, 64)));
            hitUp.AddFrame(new Frame(80, new IntRect(64, 3232, 64, 64)));
            hitUp.AddFrame(new Frame(80, new IntRect(128, 3232, 64, 64)));
            hitUp.AddFrame(new Frame(80, new IntRect(192, 3232, 64, 64)));
            hitUp.AddFrame(new Frame(80, new IntRect(256, 3232, 64, 64)));
            hitUp.AddFrame(new Frame(80, new IntRect(0, 3232, 64, 64)));
            hitUp.AddFrame(new Frame(1, new IntRect(1, 3004, 64, 64)));

            var hitDown = new Animation(playerSprite);
            hitDown.AddFrame(new Frame(80, new IntRect(0, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(80, new IntRect(256, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(80, new IntRect(64, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(80, new IntRect(128, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(80, new IntRect(192, 3296, 64, 75)));

            hitDown.AddFrame(new Frame(80, new IntRect(128, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(80, new IntRect(64, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(80, new IntRect(256, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(80, new IntRect(0, 3296, 64, 64)));
            hitDown.AddFrame(new Frame(1, new IntRect(64, 3040, 64, 64)));
            
            var standUp = new Animation(playerSprite);
            
            standUp.AddFrame(new Frame(110, new IntRect(0, 3371, 64, 64)));
            standUp.AddFrame(new Frame(110, new IntRect(64, 3371, 64, 64)));
            standUp.AddFrame(new Frame(80, new IntRect(128, 3371, 64, 64)));
            standUp.AddFrame(new Frame(70, new IntRect(192, 3371, 64, 64)));
            standUp.AddFrame(new Frame(100, new IntRect(0, 3040, 64, 64)));

            return new Dictionary<Enum, Animation>
            {
                {MoveAnimationState.Left, leftAnimation},
                {MoveAnimationState.Right, rightAnimation},
                {MoveAnimationState.StandUp, standUp},
                
                {HitAnimation.HitLeft, hitLeft},
                {HitAnimation.HitRight, hitRight},
                {HitAnimation.HitUp, hitUp},
                {HitAnimation.HitDown, hitDown},
                
            };
        }

        public static Dictionary<Enum, Animation> CreateFloorAnimation(Sprite tileSprite, DungeonType dungeonType, Orientation orientation = Orientation.Simple)
        {
            var upAnimation = new Animation(tileSprite);
            switch (dungeonType)
            {
                case DungeonType.Lobby:
                    upAnimation.AddFrame(new Frame(80, new IntRect(480, 896, 32, 32)));

                    break;
                case DungeonType.DungeonGrass:
                    upAnimation.AddFrame(new Frame(80, tiles[orientation]));
                    break;

                case DungeonType.DungeonStone:
                    upAnimation.AddFrame(new Frame(80, new IntRect(0, 224, 32, 32)));
                    break;

                case DungeonType.DungeonBoss:
                    upAnimation.AddFrame(new Frame(80, new IntRect(1568, 96, 32, 32)));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dungeonType), dungeonType, null);
            }

            return new Dictionary<Enum, Animation>
            {
                [StaticAnimationState.Up] = upAnimation
            };
        }

        public static Dictionary<Enum, Animation> CreateDiggerAnimation(Sprite tileSprite, DungeonType dungeonType)
        {
            var animation = new Animation(tileSprite);
            switch (dungeonType)
            {
                case DungeonType.Lobby:
                    break;
                case DungeonType.DungeonGrass:
                    animation.AddFrame(new Frame(1, new IntRect(159, 2176, 32, 32)));
                    break;
                case DungeonType.DungeonStone:
                    animation.AddFrame(new Frame(1, new IntRect(288, 2112, 32, 32)));
                    break;
                case DungeonType.DungeonBoss:
                    animation.AddFrame(new Frame(1, new IntRect(832, 2080, 32, 32)));
                    break;
            }

            return new Dictionary<Enum, Animation>
            {
                [CommonState.Default] = animation
            };
        }

        public static Dictionary<Enum, Animation> CreateStarAnimation(Sprite tileSprite, DungeonType dungeonType)
        {
            var animation = new Animation(tileSprite);
            switch (dungeonType)
            {
                case DungeonType.Lobby:
                    break;
                case DungeonType.DungeonGrass:
                    animation.AddFrame(new Frame(1, new IntRect(1215, 1887, 32, 32)));
                    break;
                case DungeonType.DungeonStone:
                    animation.AddFrame(new Frame(1, new IntRect(1185, 1888, 32, 32)));
                    break;
                case DungeonType.DungeonBoss:
                    break;
            }

            return new Dictionary<Enum, Animation>
            {
                [CommonState.Default] = animation
            };

        }

        public static Dictionary<Enum, Animation> CreateWallAnimation(Sprite tileSprite, DungeonType dungeonType)
        {
            var upAnimation = new Animation(tileSprite);

            switch (dungeonType)
            {
                case DungeonType.Lobby:
                    upAnimation.AddFrame(new Frame(1, new IntRect(448, 896, 32, 32)));

                    break;
                case DungeonType.DungeonGrass:
                    upAnimation.AddFrame(new Frame(1, new IntRect(1760, 416, 32, 32)));
                    break;

                case DungeonType.DungeonStone:
                    upAnimation.AddFrame(new Frame(1, new IntRect(672, 512, 32, 32)));
                    break;

                case DungeonType.DungeonBoss:
                    upAnimation.AddFrame(new Frame(1, new IntRect(320, 96, 32, 32)));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dungeonType), dungeonType, null);
            }

            return new Dictionary<Enum, Animation>
            {
                [StaticAnimationState.Up] = upAnimation
                // [State.LeftUpCorner] = cornerAnimation
                
            };
        }

        public static Dictionary<Enum, Animation> CreateLevelSelectorAnimation(Sprite tileSprite,
            DungeonType dungeonType)
        {
            var animation = new Animation(tileSprite);
            // todo: в зависимости от тип данжа добавить эффекты (подпись снизу)
            animation.AddFrame(new Frame(1, new IntRect(1696, 352, 32, 32)));

            return new Dictionary<Enum, Animation>
            {
                [StaticAnimationState.Up] = animation
            };
        }

        public enum CommonState
        {
            Default = 60
        }

        public enum RageState
        {
            InRange = 50
        }

        // todo: add spawn animation
        public enum MoveAnimationState
        {
            Left = 0, Right = 1, StandUp = 2
        }

        public enum HitAnimation
        {
            HitLeft = 10, HitRight = 11, HitUp = 12, HitDown = 13
        }

        public enum StaticAnimationState
        {
            Up = 20, Down = 21, Left = 22, Right = 23,
            UpLeftCorner = 30, UpRightCorner = 31, DownLeftCorner = 32, DownRightCorner = 33
        }

        public enum DungeonType
        {
            Lobby = 40, DungeonGrass = 41, DungeonStone = 42, DungeonBoss = 43
        }

        public enum Orientation
        {
            BottomRightCorner, BottomLeftCorner, TopRightCorner, TopLeftCorner,
            TopDeadEnd, BottomDeadEnd, LeftDeadEnd, RightDeadEnd,
            Ring, VerticalPassage, HorizontalPassage, RightSingle,
            LeftSingle, TopSingle, BottomSingle, Simple
        }

        public static Dictionary<Orientation, IntRect> tiles = new Dictionary<Orientation, IntRect>()
        {
            { Orientation.BottomLeftCorner,  new IntRect(1376, 288, 32, 32) },
            { Orientation.BottomRightCorner, new IntRect(1568, 288, 32, 32) },
            { Orientation.BottomSingle,      new IntRect( 672, 288, 32, 32) },
            { Orientation.LeftSingle,        new IntRect(1504, 288, 32, 32) },
            { Orientation.RightSingle,       new IntRect(1696, 288, 32, 32) },
            { Orientation.TopLeftCorner,     new IntRect(1440, 288, 32, 32) },
            { Orientation.TopRightCorner,    new IntRect(1632, 288, 32, 32) },
            { Orientation.TopSingle,         new IntRect(1760, 288, 32, 32) },
            { Orientation.VerticalPassage,   new IntRect(1312, 288, 32, 32) },
            { Orientation.Ring,              new IntRect(1312, 288, 32, 32) },
            { Orientation.TopDeadEnd,        new IntRect(1312, 288, 32, 32) },
            { Orientation.RightDeadEnd,      new IntRect(1312, 288, 32, 32) },
            { Orientation.BottomDeadEnd,     new IntRect(1312, 288, 32, 32) },
            { Orientation.HorizontalPassage, new IntRect(1312, 288, 32, 32) },
            { Orientation.LeftDeadEnd,       new IntRect(1312, 288, 32, 32) },
            { Orientation.Simple,            new IntRect(1312, 288, 32, 32) },
        };
    }
}