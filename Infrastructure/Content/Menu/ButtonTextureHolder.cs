using SFML.Graphics;

namespace RogueLikeGame0._2.Infrastructure.Content.Menu
{
    public class ButtonTextureHolder
    {
        public Texture texture;
        public IntRect normalRect;
        public IntRect selectedRect;
        public IntRect pressedRect;   
    }
}