using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using SFML.Graphics;

namespace RogueLikeGame0._2.Infrastructure
{
    public interface IGameContext
    {
        Sprite GetSprite();
        IBehaviour GetTactic<T>(T obj, params object[] args);
    }
}