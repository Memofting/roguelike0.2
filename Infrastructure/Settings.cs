using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Infrastructure
{
    public struct Settings
    {
        public const int StarHealthLowerBound = -1;
        public const int PlayerHealthLowerBound = 0;

        public const int StarAttack = 1;
        public const int PlayerAttack = 1;
        
        public const int CellWidth = 64;
        public const int CellHeight = 64;

        public static readonly Color TransparentRed = new Color(255, 0, 0);


        public static readonly (Vector2i, Vector2f) MoveUp = (new Vector2i(0, -1), new Vector2f(0, -CellHeight));
        public static readonly (Vector2i, Vector2f) MoveDown = (new Vector2i(0, 1), new Vector2f(0, CellHeight));
        public static readonly (Vector2i, Vector2f) MoveLeft = (new Vector2i(-1, 0), new Vector2f(-CellWidth, 0));
        public static readonly (Vector2i, Vector2f) MoveRight = (new Vector2i(1, 0), new Vector2f(CellWidth, 0));
        public static readonly (Vector2i, Vector2f) MoveOnPlace = (new Vector2i(0, 0), new Vector2f(0, 0));

        public enum MoveDirection
        {
            Up,
            Down,
            Right,
            Left
        }

        public enum HitDirection
        {
            Up,
            Down,
            Right,
            Left
        }
    }
}