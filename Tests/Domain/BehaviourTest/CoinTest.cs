using System.Linq;
using NUnit.Framework;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Domain.Implementation;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Tests.Domain.BehaviourTest.TestUtils;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Tests.Domain.BehaviourTest
{
    [TestFixture]
    public class CoinTest
    {
        [Test]
        public void TestLiftUpCoin()
        {
            var scene = new Scene(
                new ControlledTestContext(), 
                Content.DungeonType.Lobby,
                SceneWithAllyInstanceAndCoin);
            var game = new Game(
                new DungeonMaster(new View(), new View()),  
                null, null, scene);
            Assert.AreEqual(1, scene.GetEntities<IAlly>().Count(), "Should be exactly one ally");
            Assert.AreEqual(1, scene.GetEntities<Coin>().Count(), "Should be exactly one coin");

            var coinPosition = scene.GetEntities<Coin>().First().PositionComponent.OnScene;

            game.UpdateLogic();

            Assert.AreEqual(
                coinPosition,
                scene.GetEntities<ControlledAlly>().First().PositionComponent.OnScene,
                "Ally should stand at the coin's position after lifting it up.");
            Assert.AreEqual(0, scene.GetEntities<Coin>().Count(), "After ally moving coin should be lifted up.");
            Assert.Greater(
                scene.GetEntities<IAlly>().First().ScoreComponent.Score,
                0,
                "Score should be incremented after lifting coin up");
        }

        [Test]
        public void TestMobsDoNotTakeCoins()
        {
            var scene = new Scene(
                new ControlledTestContext(), 
                Content.DungeonType.DungeonGrass,
                SceneWithMobInstanceAndCoin);
            var game = new Game(
                new DungeonMaster(new View(), new View()),  
                null, null, scene);
            
            Assert.AreEqual(1, scene.GetEntities<IAlly>().Count(), "Should be exactly one ally");
            Assert.AreEqual(1, scene.GetEntities<Coin>().Count(), "Should be exactly one coin");
            Assert.AreEqual(1, scene.GetEntities<IEnemy>().Count(), "Should be exactly one mob");

            var coinPosition = scene.GetEntities<Coin>().First().PositionComponent.OnScene;

            game.UpdateLogic();
            Assert.AreEqual(1, scene.GetEntities<Coin>().Count());
            Assert.AreEqual(
                new Vector2i(0, 1),
                scene.GetEntities<ControlledEnemy> ().First().PositionComponent.OnScene,
                "Enemy move to ally");

            game.UpdateLogic();
            Assert.AreEqual(1, scene.GetEntities<Coin>().Count());
            Assert.AreEqual(
                new Vector2i(0, 1),
                scene.GetEntities<ControlledEnemy>().First().PositionComponent.OnScene,
                "Enemy do not take coin");
            Assert.AreEqual(
                coinPosition,
                scene.GetEntities<Coin>().First().PositionComponent.OnScene);
        }

        private (IStaticEntity[,], IActiveEntity[,]) SceneWithAllyInstanceAndCoin(IGameContext context, Content.DungeonType dungeonType)
        {
            return (
                new IStaticEntity[1, 2],
                new IActiveEntity[,]
                {
                    {
                        new ControlledAlly(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.Lobby, new Vector2i(0, 1)), 
                        new Coin(context, new Vector2i(0, 1), new Vector2f(0, 1), Content.DungeonType.Lobby),
                    }
                });
        }
        private (IStaticEntity[,], IActiveEntity[,]) SceneWithMobInstanceAndCoin(IGameContext context, Content.DungeonType dungeonType)
        {
            return (
                new IStaticEntity[, ]
                {
                    {
                        new Wall(context, new Vector2i(0, 3), new Vector2f(0, 3), Content.DungeonType.DungeonGrass), 
                        new Floor(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.DungeonGrass), 
                        new Floor(context, new Vector2i(0, 1), new Vector2f(0, 1), Content.DungeonType.DungeonGrass), 
                        new Floor(context, new Vector2i(0, 4), new Vector2f(0, 4), Content.DungeonType.DungeonGrass), 
                        new Floor(context, new Vector2i(0, 2), new Vector2f(0, 2), Content.DungeonType.DungeonGrass), 
                    }
                },
                new IActiveEntity[,]
                {
                    {
                        new ControlledAlly(context, new Vector2i(0, 4), new Vector2f(0, 4), Content.DungeonType.DungeonGrass, new Vector2i(0, 1)),
                        new ControlledEnemy(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.DungeonGrass, new Vector2i(0, 1)), 
                        new Coin(context, new Vector2i(0, 2), new Vector2f(0, 2), Content.DungeonType.DungeonGrass),

                    }
                });
        }
    }
}