using NUnit.Framework;
using RogueLikeGame0._2.Domain.Implementation;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using SFML.Graphics;
using SFML.System;
using System.Linq;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Tests.Domain.BehaviourTest.TestUtils;

namespace RogueLikeGame0._2.Tests.Domain.BehaviourTest
{
    public class EnemyTest
    {
        [Test]
        public void TestMobNotDyingByHitingThemselves()
        {
            var scene = new Scene(
                new ControlledTestContext(), 
                Content.DungeonType.Lobby,
                SceneWithMobInstance);
            var game = new Game(
                new DungeonMaster(new View(), new View()), null, null, scene);

            Assert.AreEqual(1, scene.GetEntities<IAlly>().Count(), "Should be exactly one ally");
            Assert.AreEqual(1, scene.GetEntities<IEnemy>().Count(), "Should be exactly one mob");
            for (var _ = 0; _ < 20; ++_) game.UpdateLogic();
            
            Assert.AreEqual(1,
                scene.GetEntities<IEnemy>().Count(),
                "Emeny should not take damage by hiting themselves");
        }

        [Test]
        public void TestMobsDoNotKillEachOther()
        {
            var scene = new Scene(
                new ControlledTestContext(), 
                Content.DungeonType.Lobby,
                SceneWithTwoMobInstance);
            var game = new Game(
                new DungeonMaster(new View(), new View()), null, null, scene);

            Assert.AreEqual(1, scene.GetEntities<IAlly>().Count(), "Should be exactly one ally");
            Assert.AreEqual(2, scene.GetEntities<IEnemy>().Count(), "Should be exactly two mobs");
            for (var _ = 0; _ < 20; ++_) game.UpdateLogic();

            Assert.AreEqual(2,
                scene.GetEntities<IEnemy>().Count(),
                "Emeny should not take damage by hiting each other");
        }

        private (IStaticEntity[,], IActiveEntity[,]) SceneWithMobInstance(IGameContext context,
            Content.DungeonType dungeonType)
        {
            return (
                new IStaticEntity[,]
                {
                    {
                        new Wall(context, new Vector2i(0, 3), new Vector2f(0, 3), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 1), new Vector2f(0, 1), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 4), new Vector2f(0, 4), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 2), new Vector2f(0, 2), Content.DungeonType.DungeonGrass),
                    }
                },
                new IActiveEntity[,]
                {
                    {
                        new ControlledAlly(context, new Vector2i(0, 4), new Vector2f(0, 4),
                            Content.DungeonType.DungeonGrass, new Vector2i(0, 1)),
                        new ControlledEnemy(context,new Vector2i(0, 0), new Vector2f(0, 0),
                            Content.DungeonType.DungeonGrass, new Vector2i(0, 1)),
                    }
                });
        }
        private (IStaticEntity[,], IActiveEntity[,]) SceneWithTwoMobInstance(IGameContext context,
            Content.DungeonType dungeonType)
        {
            return (
                new IStaticEntity[,]
                {
                    {
                        new Wall(context, new Vector2i(0, 3), new Vector2f(0, 3), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 1), new Vector2f(0, 1), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 4), new Vector2f(0, 4), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 2), new Vector2f(0, 2), Content.DungeonType.DungeonGrass),
                    }
                },
                new IActiveEntity[,]
                {
                    {
                        new ControlledAlly(context, new Vector2i(0, 4), new Vector2f(0, 4),
                            Content.DungeonType.DungeonGrass, new Vector2i(0, 1)),
                        new ControlledEnemy(context, new Vector2i(0, 0), new Vector2f(0, 0),
                            Content.DungeonType.DungeonGrass, new Vector2i(1, 0)),
                        new ControlledEnemy(context, new Vector2i(0, 1), new Vector2f(0, 1),
                            Content.DungeonType.DungeonGrass, new Vector2i(0, -1)),
                    }
                });
        }
    }
}