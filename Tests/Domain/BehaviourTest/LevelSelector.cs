﻿using System.Linq;
using NUnit.Framework;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Domain.Implementation;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Tests.Domain.BehaviourTest.TestUtils;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Tests.Domain.BehaviourTest
{
    [TestFixture]
    public class LevelSelectorTest
    {
        [Test]
        public void TestMobsDoNotStayOnLevelSelector()
        {
            var scene = new Scene(
                new ControlledTestContext(), 
                Content.DungeonType.DungeonGrass,
                SceneWithMobInstanceAndCoinAndLevelSelector);
            var game = new Game(
                new DungeonMaster(new View(), new View()), null, null, scene);

            Assert.AreEqual(1, scene.GetEntities<IAlly>().Count(), "Should be exactly one ally");
            Assert.AreEqual(1, scene.GetEntities<LevelSelector>().Count(), "Should be exactly one level selector");
            Assert.AreEqual(1, scene.GetEntities<IEnemy>().Count(), "Should be exactly one boss");

            var lvlSelectorPosition = scene.GetEntities<LevelSelector>().First().PositionComponent.OnScene;

            game.UpdateLogic();

            Assert.AreEqual(
                lvlSelectorPosition,
                scene.GetEntities<ControlledEnemy>().First().PositionComponent.OnScene,
                "No one should ever know this is not working!");
            Assert.AreEqual(1, scene.GetEntities<LevelSelector>().Count());
        }

        private (IStaticEntity[,], IActiveEntity[,]) SceneWithMobInstanceAndCoinAndLevelSelector(IGameContext context, Content.DungeonType dungeonType)
        {
            return (
                new IStaticEntity[,]
                {
                    {
                        new Wall(context, new Vector2i(0, 2), new Vector2f(0, 2), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 1), new Vector2f(0, 1), Content.DungeonType.DungeonGrass),
                        new Floor(context, new Vector2i(0, 3), new Vector2f(0, 3), Content.DungeonType.DungeonGrass),
                    }
                },
                new IActiveEntity[,]
                {
                    {
                        new ControlledAlly(context, new Vector2i(0, 3), new Vector2f(0, 3), Content.DungeonType.DungeonGrass, new Vector2i(0, 1)),
                        new ControlledEnemy(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.DungeonGrass, new Vector2i(0, 1)), 
                        new LevelSelector(context, new Vector2i(0, 1), new Vector2f(0, 1), Content.DungeonType.DungeonGrass),
                    }
                });
        }
    }
}