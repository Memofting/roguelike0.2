﻿using RogueLikeGame0._2.Domain.Implementation;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.FightLogic;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using SFML.Graphics;
using SFML.System;
using System.Collections.Generic;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;

namespace RogueLikeGame0._2.Tests.Domain.BehaviourTest.TestUtils
{   
    public class ControlledAlly : GameObject, IAlly, IActiveEntity, IMovable, IControlledObject
    {
        public ControlledAlly(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType, Vector2i dirVector)
            : base(context, position, onScene, dungeonType)
        {
            DicVector2I = dirVector;

            var tactics = context.GetTactic(this);
            
            MoveComponent = new MoveComponent(this, tactics);
            ScoreComponent = new ScoreComponent(this, 0);
        }

        public override void Update(IScene scene)
        {
            MoveComponent.Update(scene);
        }

        public ScoreComponent ScoreComponent { get; }
        public MoveComponent MoveComponent { get; }
        public Vector2i DicVector2I { get; }
    }
    
    public class ControlledTestContext : IGameContext
    {
        public Sprite GetSprite()
        {
            return new Sprite();
        }

        public IBehaviour GetTactic<T>(T obj, params object[] args)
        {
            if (typeof(T) == typeof(ControlledAlly))
                return new BehaviourContainer(new ControlledAllyTactic(obj as IControlledObject));
            if (typeof(T) == typeof(ControlledEnemy))
                return new BehaviourContainer(new ControlledEnemyTactic(obj as IControlledObject), new TryingHitItselfEnemyTactic());
            return null;
        }
    }

    public class ControlledAllyTactic : IMoveTactic
    {
        private IControlledObject _controlledObject;

        public ControlledAllyTactic(IControlledObject controlledObject) => _controlledObject = controlledObject;
        
        public Vector2i GetShiftForNextMove()
        {
            return _controlledObject.DicVector2I;
        }
    }

    public interface IControlledObject
    {
        Vector2i DicVector2I { get; }
    }

    public class ControlledEnemy : GameObject, IEnemy, IActiveEntity, IMovable, IDestructive, IDestroyable, IControlledObject
    {
        public Vector2i DicVector2I { get; }
        public ControlledEnemy(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType, Vector2i direcVector2I)
            : base(context, position, onScene, dungeonType)
        {
            DicVector2I = direcVector2I;
            var tactic = context.GetTactic(this);
            MoveComponent = new MoveComponent(this, tactic);
            DestructiveComponent = new DestructiveComponent(this, tactic);
            
            
            DestroyableComponent = new DestroyableComponent(this,
                4,
                scene => DestroyableComponent.Health += 1,
                scene =>
                {
                    DestroyableComponent.Health -= 1;
                },
                scene =>
                {
                    scene.ActiveGameObjects[PositionComponent.OnScene.X, PositionComponent.OnScene.Y] = null;

                    var coin = new Coin(context, PositionComponent.OnScene, DrawComponent.Position, DungeonType);
                    coin.SpawnComponent.Update(scene);
                });
        }

        public override void Update(IScene scene)
        {
            MoveComponent.Update(scene);
        }

        public MoveComponent MoveComponent { get; }
        public DestroyableComponent DestroyableComponent { get; }
        public DestructiveComponent DestructiveComponent { get; }
    }
    
    public class ControlledEnemyTactic : IMoveTactic
    {
        private readonly IControlledObject _observableObject;

        public ControlledEnemyTactic(IControlledObject controlledObject) => _observableObject = controlledObject;

        public Vector2i GetShiftForNextMove()
        {
            return _observableObject.DicVector2I;
        }
    }
    
    public class TryingHitItselfEnemyTactic : IHitTactic
    {
        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            yield return new Vector2i(0, 0);
        }
    }
}
