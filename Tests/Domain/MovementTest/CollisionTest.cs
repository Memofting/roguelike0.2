using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Domain.Implementation;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Domain.Logic.TacticsInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using RogueLikeGame0._2.Logic;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Tests.Domain.MovementTest
{
    [TestFixture]
    public class SlipTest
    {
        private Game game;
        private Scene scene;
        private DungeonMaster dungeonMaster;

        private char[,] sceneMap = {
            {' ', '0', ' '},
            {' ', ' ', ' '},
            {' ', '0', ' '},
        };

        private (IStaticEntity[,], IActiveEntity[,]) SceneInitializer(IGameContext context, Content.DungeonType dungeonType,
            Func<object[], IActiveEntity> producer)
        {
            var staticMap = new IStaticEntity[3, 3];
            var activeMap = new IActiveEntity[3, 3];

            for (var i = 0; i < 3; ++i)
                for (var j = 0; j < 3; ++j)
                {
                    staticMap[i, j] = new Floor(context, new Vector2i(i, j), new Vector2f(i * 32, j + 32), dungeonType);
                    if (sceneMap[i, j] == '0')
                        activeMap[i, j] = producer(new object[] { context, new Vector2i(i, j), new Vector2f(i * 32, j + 32), dungeonType });
                }

            return (staticMap, activeMap);
        }

        [Test]
        public void TestNotSlipWhenMoveTwoEntitiesInSamePlace()
        {

            dungeonMaster = new DungeonMaster(new View(), new View());
            scene = new Scene(
                new ColissionContext(), 
                Content.DungeonType.Lobby,
                (context, type) => SceneInitializer(context, type, Level.CreateInstance<CenterMob>));
            game = new Game(dungeonMaster, new View(), new View(), scene);
//            game.ChangeScene(scene);

            Assert.AreEqual(2,
                scene.GetEntities<CenterMob>().Count(),
                "Scene should be initialized with 2 GameObjects");


            game.UpdateLogic();

            Assert.AreEqual(
                2, scene.GetEntities<CenterMob>().Count(),
                "Should be alive all GameObjects");
            Assert.IsTrue(scene.GetEntities<CenterMob>()
                    .Any(s => s.PositionComponent.OnScene.X == 1
                              && s.PositionComponent.OnScene.Y == 1),
                "At least one object should be moved to the center of scene");
        }

        [Test]
        public void TestNotSlipWhenMoveTwoEntitiesInSamePlaceWithHit()
        {
            dungeonMaster = new DungeonMaster(new View(), new View());
            scene = new Scene(
                new ColissionContext(),
                Content.DungeonType.Lobby,
                (master, type) => SceneInitializer(master, type, Level.CreateInstance<CenterMobHit>));
            game = new Game(dungeonMaster, new View(), new View(), scene);

            Assert.AreEqual(
                2, scene.GetEntities<CenterMobHit>().Count(),
                "Scene should be initialized with 2 GameObjects");

            game.UpdateLogic();

            Assert.AreEqual(
                1, scene.GetEntities<CenterMobHit>().Count(),
                "Should be alive only one GameObject");

            Assert.IsTrue(scene.GetEntities<CenterMobHit>()
                    .Any(s => s.PositionComponent.OnScene.X != 1
                              || s.PositionComponent.OnScene.Y != 1),
                "Alive object should not be moved to the center of scene");
        }
    }

    public class ColissionContext : IGameContext
    {
        public Sprite GetSprite()
        {
            return new Sprite();
        }

        public IBehaviour GetTactic<T>(T obj, params object[] args)
        {
            if (typeof(T) == typeof(CenterMob))
                return new BehaviourContainer(new CenterMoveTactic(obj as IHasPosition));
            if (typeof(T) == typeof(CenterMobHit))
                return new BehaviourContainer(new CenterMoveTactic(obj as IHasPosition), new CenterHitTactic());
            return null;
        }
    }

    public class CenterMoveTactic : IMoveTactic
    {
        private IHasPosition _observable;

        public CenterMoveTactic(IHasPosition observable)
        {
            _observable = observable;
        }

        public Vector2i GetShiftForNextMove()
        {
            return new Vector2i(1 - _observable.PositionComponent.OnScene.X, 0);
        }
    }

    public class CenterHitTactic : IHitTactic
    {
        public IEnumerable<Vector2i> GetHitObjectsLocations()
        {
            yield return new Vector2i(1, 1);
        }
    }

    // todo: оnделить IDrawable от GameObject
    public class CenterMob : GameObject, IActiveEntity, IRigid, IMovable
    {
        public CenterMob(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType)
            : base(context, position, onScene, dungeonType)
        {
            var tactic = context.GetTactic(this);
            MoveComponent = new MoveComponent(this, tactic);

//            MoveComponent = new MoveComponent(this, new CenterMoveTactic(this));
//            DestructiveComponent = new DestructiveComponent(this, new CenterHitTactic());
//            MoveComponent = new MoveComponent(this, new CenterMoveTactic(this));
        }

        public override void Update(IScene scene)
        {
            MoveComponent.Update(scene);
        }
        public MoveComponent MoveComponent { get; }
    }

    public class CenterMobHit : GameObject, IActiveEntity, IRigid, IMovable, IDestroyable, IDestructive
    {
        public CenterMobHit(IGameContext context, Vector2i onScene, Vector2f position, Content.DungeonType dungeonType)
            : base(context, position, onScene, dungeonType)
        {
            var tactic = context.GetTactic(this);
            
//            MoveComponent = new MoveComponent(this, new CenterMoveTactic(this));
//            DestructiveComponent = new DestructiveComponent(this, new CenterHitTactic());


            MoveComponent = new MoveComponent(this, tactic);
            DestructiveComponent = new DestructiveComponent(this, tactic);


            DestroyableComponent = new DestroyableComponent(
                this, 1,
                scene => DestroyableComponent.Health += 1,
                scene => DestroyableComponent.Health -= 1,
                scene => scene[PositionComponent.OnScene] = null);
        }

        public override void Update(IScene scene)
        {
            MoveComponent.Update(scene);
            DestructiveComponent.Update(scene);
        }

        public MoveComponent MoveComponent { get; }
        public DestroyableComponent DestroyableComponent { get; }
        public DestructiveComponent DestructiveComponent { get; }
    }
}