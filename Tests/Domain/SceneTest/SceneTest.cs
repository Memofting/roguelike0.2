using System.Linq;
using NUnit.Framework;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Domain.Implementation.EntitiesClasses;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.Logic.EntitiesInterfaces;
using RogueLikeGame0._2.Infrastructure;
using RogueLikeGame0._2.Infrastructure.Content;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.Tests.Domain.SceneTest
{
    [TestFixture]
    public class SceneTest
    {

        [Test]
        public void TestCorrectFindPlayer()
        {
            var scene = new Scene(
                new GameContext(), 
                Content.DungeonType.Lobby,
                SceneWithPlayer);
            Assert.IsNotNull(scene.GetEntities<Player>().FirstOrDefault(), "Can't find player on scene");
        }

        [Test]
        public void TestNullOnPlayerMissing()
        {
            var scene = new Scene(
                new GameContext(), 
                Content.DungeonType.Lobby,        
                SceneWithoutPlayer);
            Assert.IsNull(scene.GetEntities<Player>().FirstOrDefault(), "Shouldn't find player and return null");
        }

        private (IStaticEntity[,], IActiveEntity[,]) SceneWithPlayer(IGameContext context, Content.DungeonType dungeonType)
        {
            return (
                    new IStaticEntity[0, 0],
                    new IActiveEntity[,]
                    {
                        {new Player(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.Lobby, null)}
                    });
        }

        private (IStaticEntity[,], IActiveEntity[,]) SceneWithoutPlayer(IGameContext context, Content.DungeonType dungeonType)
        {
            return (
                new IStaticEntity[0, 0],
                new IActiveEntity[,]
                {
                    {new Star(context, new Vector2i(0, 0), new Vector2f(0, 0), Content.DungeonType.Lobby) }
                });
        }
    }
}