using System;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.Masters;
using RogueLikeGame0._2.Application.RequestState;
using RogueLikeGame0._2.Domain.Implementation;
using RogueLikeGame0._2.Domain.Implementation.SceneClasses;
using RogueLikeGame0._2.Domain.MiniBar;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace RogueLikeGame0._2.UserInterface
{
    internal class DungeonUI : IApplicationHandler
    {
        private readonly double _logicUpdateFreq;
        private double _counter;
        private readonly DungeonMaster _dungeonMaster;

        private readonly View _gameView;
        private readonly View _miniMapView;
        private readonly MiniBar _miniBar;
        private readonly RenderWindow _win;

        private readonly View _barView;

        public DungeonUI(RenderWindow win, double logicUpdateFreq = 500)
        {
            _counter = 0;
            _logicUpdateFreq = logicUpdateFreq;
            _win = win;

            _gameView = new View(new FloatRect(0, 0, _win.Size.X, _win.Size.Y))
            {
                Viewport = new FloatRect(0f, 0f, 1f, 1f)
            };

            _miniMapView = new View(new FloatRect(0, 0, _win.Size.X * 2, _win.Size.Y * 2))
            {
                Viewport = new FloatRect(0.8f, 0.8f, 0.2f, 0.2f)
            };

            _barView = new View(new FloatRect(0, 0, _win.Size.X, _win.Size.Y * 0.25f))
            {
                Viewport = new FloatRect(0f, 0f, 1f, 0.25f)
            };
            _dungeonMaster = new DungeonMaster(_gameView, _miniMapView);
            _miniBar = new MiniBar();
        }

        private void Drawing()
        {
            _win.Clear(Color.Black);

            _win.SetView(_gameView);
            _win.Draw(_dungeonMaster);

            _win.SetView(_miniMapView);
            _win.Draw(_dungeonMaster);

            _win.SetView(_barView);
            _win.Draw(_miniBar);

            _win.Display();
        }

        public bool Update(Time time)
        {
            _counter += time.AsMilliseconds();

            if (_counter >= _logicUpdateFreq)
            {
                _win.DispatchEvents();
                _dungeonMaster.UpdateLogic();
                _counter %= _logicUpdateFreq;
            }

            _dungeonMaster.UpdateAnimation(time.AsMilliseconds());

            Drawing();
            return _dungeonMaster.TryStopGame();
        }
    
        public void HandleEvent(object sender, EventArgs eventArgs)
        {
            _dungeonMaster.HandleEvent(sender, eventArgs);
        }
    }
}