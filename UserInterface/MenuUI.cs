using System;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.RequestState;
using RogueLikeGame0._2.Domain.Gui;
using RogueLikeGame0._2.Infrastructure.Content;
using SFML.Graphics;
using SFML.System;

namespace RogueLikeGame0._2.UserInterface
{
    public class MenuUI : IApplicationHandler
    {
        private readonly Context _mContext;
        private readonly RenderWindow _mWin;
        private readonly Container _mGuiContainer;

        public MenuUI(RenderWindow win, Context context)
        {
            _mWin = win;
            _mContext = context;
            _mGuiContainer = new Container();

            var creditButton = new Button(Content.MenuFont, Content.CreateButtonTextureHolder())
            {
                Position = new Vector2f(100, 250)
            };
            creditButton.SetText("Credits");
            creditButton.CallBack += () => Console.WriteLine("Game credits");

            
            var playButton = new Button(Content.MenuFont, Content.CreateButtonTextureHolder())
            {
                Position = new Vector2f(100, 200)
            };
            playButton.SetText("Play");
            playButton.CallBack += () =>
            {
                Console.WriteLine("Game start!");
                
                GameMaster.RequestStack.RegisterStateHandler(ApplicationState.Dungeon, new DungeonUI(win));
                GameMaster.RequestStack.Push(ApplicationState.Dungeon);
            };


            var exitButton = new Button(Content.MenuFont, Content.CreateButtonTextureHolder())
            {
                Position = new Vector2f(100, 300)
            };
            exitButton.SetText("Exit");
            exitButton.CallBack += () => _mWin.Close();
            
            var label = new Label("WISDOM CAVE", Content.MenuFont)
            {
                Position = new Vector2f(250, 100)
            };
            
            _mGuiContainer.Pack(label);
            _mGuiContainer.Pack(playButton);
            _mGuiContainer.Pack(creditButton);
            _mGuiContainer.Pack(exitButton);

            _mWin.SetVerticalSyncEnabled(true);
        }

        public bool Update(Time time)
        {
            _mWin.DispatchEvents();
            _mWin.Clear(Color.Black);
            
            _mWin.SetView(_mWin.DefaultView);
            _mWin.Draw(_mContext.MBackGroundSprite);
            _mWin.Draw(_mGuiContainer);
            
            _mWin.Display();
            return _mWin.IsOpen;
        }

        public void HandleEvent(object sender, EventArgs eventArgs)
        {
            _mGuiContainer.HandleEvent(eventArgs);
        }
    }

    public struct Context
    {
        public readonly Sprite MBackGroundSprite;

        public Context(Sprite mBackGroundSprite)
        {
            MBackGroundSprite = new Sprite(mBackGroundSprite);
        }
    }
}