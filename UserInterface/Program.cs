﻿using SFML.Graphics;
using SFML.Window;
using RogueLikeGame0._2.Application;
using RogueLikeGame0._2.Application.RequestState;
using RogueLikeGame0._2.Infrastructure.Content;

namespace RogueLikeGame0._2.UserInterface
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var win = new RenderWindow(new VideoMode(1080, 840), "Hyper game!");
            win.SetVerticalSyncEnabled(true);
            win.KeyPressed += GameMaster.OnEvent;            
            
            GameMaster.RequestStack.RegisterStateHandler(
                ApplicationState.Menu, new MenuUI(win, new Context(Content.CreateColoredSprite(Color.White))));
            GameMaster.RequestStack.Push(ApplicationState.Menu);

            
            win.Closed += (sender, evArgs) =>
            {
                win.Close();
                while (GameMaster.RequestStack.TryPop(out _))
                {
                }
            };
            win.Resized += (sender, evArgs) => win.SetView(win.DefaultView);
            GameMaster.Run();
        }
    }
}